#!/bin/bash

service apache2 start

# A hack to keep the Docker image alive, since the above are forked
tail -f /dev/null


