#!/bin/bash

(cd halogen-amd64; make build-nocache)

(cd halogen-mkpublisher-amd64; make build)

(cd halogen-mksubscriber-amd64; make build)

