#!/bin/bash

# Sometimes, a image rebuild creates a postgres user with a different numeric ID!
# In this case, PostgreSQL won't start unless all its directories are owned by the correct user.
chown -R postgres:postgres /mnt/data/etc/postgresql /mnt/data/var/log/postgresql /mnt/data/var/lib/postgresql

service postgresql start

# A hack to keep the Docker image alive, since the above are forked
tail -f /dev/null

