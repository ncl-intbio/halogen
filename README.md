# Halogen #


## Quick-start ##

Halogen consists of several independent components. We have provided Docker images for each component. The following commands will get you an extremely basic Halogen instance up and running, which you can use for experimentation or learning about the system.


```
#!bash
cd examples/halogendb
make rund

cd ../halogen-publisher-ws
make rund

cd ../halogen-subscriber-ws
make rund
```

To start a simple subscriber, run:

```
#!bash
./HelloHalogenSubscriber.sh
```

Then to publish your first message, run:

```
#!bash
./HelloHalogenPublisher.sh
```