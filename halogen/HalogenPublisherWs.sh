#!/bin/bash

EXE="com.microbasecloud.halogen.publisher.HalPublisherWsLauncher"

if [ -z $RAM ] ; then
  RAM="-Xmx256M"
  echo "Using default RAM setting: $RAM"
fi


DIR="$(dirname "$(readlink -f "$0")")"

CP="${DIR}/target/dependency/*"
CP="$CP:${DIR}/target/classes"


# Debug
#java $RAM \
#    -XX:+UnlockCommercialFeatures -XX:+FlightRecorder -Dcom.sun.management.jmxremote \
#    -Dcom.sun.management.jmxremote.port=6062 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false \
#    -cp $CP $EXE $@ 

java $RAM -cp $CP $EXE $@ 


