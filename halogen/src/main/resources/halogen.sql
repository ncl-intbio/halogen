-- Stores all messages published to the system
CREATE TABLE IF NOT EXISTS messages (
  id                              SERIAL8             NOT NULL,

  uid                             UUID                NOT NULL,
  parent_uid                      UUID                ,

  publisher                       UUID                NOT NULL,

  date                            TIMESTAMP           NOT NULL,
  channel                         VARCHAR(255)        NOT NULL,
  topic                           VARCHAR(255)        ,
  type                            VARCHAR(255)        ,
  payload                         TEXT                NOT NULL
);
CREATE UNIQUE INDEX ON messages(id);
CREATE UNIQUE INDEX ON messages(uid);

-- Still under development:
-- did publisher X ever respond to message Y?
CREATE INDEX ON messages(publisher, parent_uid, id);

-- What is the last message that publisher X ever sent?
-- What is the last message that publisher X ever responded to? [use following index, but constrain to results where parent_uid != null]
CREATE INDEX ON messages(publisher, id);






-- Data about which subscribers (users) are allowed to receive messages from the system, and a
-- description of which messages they are interested in
CREATE TABLE IF NOT EXISTS subscribers (
  uid                             uuid                NOT NULL,
  version                         bigint              NOT NULL,
  updated_at                      bigint              NOT NULL,
  updated_at_date                 timestamp           NOT NULL,
  update_id                       uuid                ,
  type                            VARCHAR(255)        NOT NULL,
  data                            TEXT                NOT NULL
);
CREATE UNIQUE INDEX ON subscribers(uid);


-- Data about which publishers (users) are allowed to receive messages from the system
CREATE TABLE IF NOT EXISTS publishers (
  uid                             uuid                NOT NULL,
  version                         bigint              NOT NULL,
  updated_at                      bigint              NOT NULL,
  updated_at_date                 timestamp           NOT NULL,
  update_id                       uuid                ,
  type                            VARCHAR(255)        NOT NULL,
  --data                            jsonb               NOT NULL
  data                            TEXT                NOT NULL
);
CREATE UNIQUE INDEX ON publishers(uid);

