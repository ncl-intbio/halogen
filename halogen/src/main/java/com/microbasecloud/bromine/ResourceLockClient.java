/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.bromine;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class ResourceLockClient {
  private HalSubscriber subscriber;
  private ResourceLockModel model;

  private Map<UUID, LockCallback> watching;


  public ResourceLockClient(HalSubscriber subscriber, ResourceLockModel model) {
    this.subscriber = subscriber;
    this.model = model;
    this.watching = new HashMap<>();
  }

  public void addWatch(UUID resourceId, LockCallback callback) {
    if (watching.containsKey(resourceId)) {
      throw new IllegalArgumentException(String.format("Resource %s is already being watched!", resourceId.toString()));
    }
    watching.put(resourceId, callback);




    // TODO publish lock request here or elsewhere?



  }

  private void processMessage(Message<ResourceLockRequest> message) {
    UUID resourceId = message.getContent().getResourceId();
    if (!watching.containsKey(resourceId)) {
      // This client is not interested in this particular lock request.
      return;
    }

    LockCallback callback = watching.remove(resourceId);
    boolean granted = model.tryLock(message.getContent());
    long grantedTimeMs = granted ? message.getContent().getRequestedLockTimeMs() : 0;

    callback.lockResult(resourceId, granted, grantedTimeMs);
  }
}
