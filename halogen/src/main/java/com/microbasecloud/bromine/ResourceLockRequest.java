/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.bromine;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class ResourceLockRequest {
  private UUID resourceId;
  private long lockTimeoutMs;
  private long requestedLockTimeMs;
  private String lockType;
  private String requesterDescription;

  public ResourceLockRequest() {
  }

  public UUID getResourceId() {
    return resourceId;
  }

  public void setResourceId(UUID resourceId) {
    this.resourceId = resourceId;
  }

  public long getLockTimeoutMs() {
    return lockTimeoutMs;
  }

  public void setLockTimeoutMs(long lockTimeoutMs) {
    this.lockTimeoutMs = lockTimeoutMs;
  }

  public long getRequestedLockTimeMs() {
    return requestedLockTimeMs;
  }

  public void setRequestedLockTimeMs(long requestedLockTimeMs) {
    this.requestedLockTimeMs = requestedLockTimeMs;
  }

  public String getLockType() {
    return lockType;
  }

  public void setLockType(String lockType) {
    this.lockType = lockType;
  }

  public String getRequesterDescription() {
    return requesterDescription;
  }

  public void setRequesterDescription(String requesterDescription) {
    this.requesterDescription = requesterDescription;
  }
}
