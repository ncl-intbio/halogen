/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.bromine;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class ResourceLockModel {
  private static final Logger logger = Logger.getLogger(ResourceLockModel.class.getName());

  private Map<String, Map<UUID, ResourceLockRequest>> workTypeToLock; // work type -> msg UUID -> Intent

  public ResourceLockModel() {
    this.workTypeToLock = new HashMap<>();
  }

  public boolean tryLock(ResourceLockRequest request) {
    // Obtain the set of locks by work type (and create it if it doesn't exist)
    Map<UUID, ResourceLockRequest> locksByWorkType = workTypeToLock.computeIfAbsent(request.getLockType(), k -> new HashMap<>());

    // Check whether there is already a lock on the target message for this type of work by some other process.
    if (locksByWorkType.containsKey(request.getResourceId())) {
      ResourceLockRequest existing = locksByWorkType.get(request.getResourceId());
      logger.info(String.format("Not locking message %s by %s for work type %s, since there is already a lock (by %s)",
          request.getResourceId(), request.getRequesterDescription(), request.getLockType(), existing.getRequesterDescription()));
      return false;
    }

    // It's ok for us to lock this message
    locksByWorkType.put(request.getResourceId(), request);
    return true;
  }

  public void unlock(ResourceLockRequest request) {
    Map<UUID, ResourceLockRequest> locksByWorkType = workTypeToLock.get(request.getLockType());
    if (locksByWorkType == null) {
      throw new RuntimeException(String.format("This is a bug. Work type: %s should exist!", request.getLockType()));
    }

    ResourceLockRequest existing = locksByWorkType.remove(request.getResourceId());
    if (existing == null) {
      throw new RuntimeException(String.format("This is a bug. There should be an intent with ID: %s!", request.getResourceId()));
    }

    if (locksByWorkType.isEmpty()) {
      workTypeToLock.remove(request.getLockType());
    }
  }
}
