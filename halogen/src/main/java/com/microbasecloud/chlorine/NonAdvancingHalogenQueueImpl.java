/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;

import java.util.*;
import java.util.logging.Logger;

/**
 * Stores a pool of messages locally, retrieved from the remote Halogen server.
 * This Halogen queue implementation has its own local message pointer, and does not in any way relay (or even alter)
 * the server-side message pointer for a given subscriber when polling.
 * Therefore, this queue has the following semantics:
 *   - Any message received by a given instance might be seen by other instances using the same subscriber ID.
 *   - You might want to think about storing the local message pointer somewhere, so that a client can resume
 *     from where it left off.
 *   - There is NO EFFECT on the remote, server-hosted message pointer for a given subscriber. Therefore, you can
 *     actually use an advancing and a non-advancing queue implementation side-by-side (if you want to).
 *
 * @author Keith Flanagan
 */
public class NonAdvancingHalogenQueueImpl<T> extends AbstractHalogenQueue<T> implements NonAdvancingHalogenQueue<T> {
  private static final Logger logger = Logger.getLogger(NonAdvancingHalogenQueueImpl.class.getName());

  private UUID messagePointer;

  public NonAdvancingHalogenQueueImpl(HalSubscriber<T> client, int maxPollSize, int maxQueueSize, UUID messagePointer) {
    super(client, maxPollSize, maxQueueSize);
    this.messagePointer = messagePointer;
  }

  public NonAdvancingHalogenQueueImpl(HalSubscriber<T> client, int maxPollSize, int maxQueueSize) {
    this(client, maxPollSize, maxQueueSize, null);
  }

  /**
   * Poll messaging server and pull new items that are not already in our local queue
   */
  @Override
  public void repopulateQueue() {
    if (!localQueue.isEmpty()) {
      return;
    }

    final int pollSize = Math.min(maxPollSize, remainingCapacity());
//    if (pollSize < minPollSize) {
//      // The local queue is nowhere near empty - it's not worth a round-trip yet.
//      return;
//    }

//    logger.info(String.format("Poll size: %s", pollSize));
    List<Message<T>> messages = client.consumeAfter(messagePointer, pollSize, false);
    addToQueue(messages);

//    logger.info(String.format("Received %s messages", messages.size()));
//    if (!messages.isEmpty()) {
//      logger.info(String.format("First message: %s", messages.get(0)));
//    }
  }

  private void addToQueue(Collection<Message<T>> messages) {
    for (Message<T> message : messages) {
      if (localQueue.size() >= maxQueueSize) {
        logger.info("It looks like an attempt was made to over-fill the local queue. This is a bug. " +
            "However, we should fail-safe here, so probably no harm done!");
        break;
      }

      boolean added = localQueue.offer(message);
      if (!added) {
        logger.info("It looks like the local queue was unexpectedly full. This is a bug. " +
            "However, we should fail-safe here, so probably no harm done!");
        break;
      }
      messagePointer = message.getHeader().getUid();
    }
  }

  @Override
  public UUID getMessagePointer() {
    return messagePointer;
  }

  @Override
  public void setMessagePointer(UUID messagePointer) {
    this.messagePointer = messagePointer;
    // We now need to clear the local queue to ensure consistency
    localQueue.clear();
  }
}
