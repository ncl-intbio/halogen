/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class State<T> {
  private static final Logger logger = Logger.getLogger(State.class.getName());

  private transient PlaybackMode playbackMode = PlaybackMode.STOPPED;
  private transient MessageConsumptionMode messageConsumptionMode = MessageConsumptionMode.MANUAL;

  /**
   * Last message that has been received and processed in 'live' mode.
   * This is useful for saving a state snapshot, and then replaying only the differences since that snapshot.
   */
  private UUID lastProcessedMessage;

  private UUID serializedAtMessage;
  private long eventsSinceLastSerialization;

  private long eventCount;
  private List<Message> recentEvents = new LinkedList<>();
  private int maxRecentEvents; // 0 to disable storage of recent events

  private T state;

  private transient EventPoller eventPoller;
  private transient EventPublisher publisher;

  private void addToEventList(List<Message> list, int maxSize, Message event) {
    if (maxSize > 0) {
      list.add(event);

      while (list.size() > maxSize) {
        list.remove(0);
      }
    }
  }

  public void addCompletedMessage(State state, Message event) {
//    if (state.getPlaybackMode() == PlaybackMode.LIVE) {
//      logger.info(String.format("Received a NEW message: %s", event.getHeader().getUid()));
//    }
//    else {
//      logger.info(String.format("While replaying history, received a message: %s", event.getHeader().getUid()));
//    }

    lastProcessedMessage = event.getHeader().getUid();
    eventCount++;
    eventsSinceLastSerialization++;

    addToEventList(recentEvents, maxRecentEvents, event);
  }

  public PlaybackMode getPlaybackMode() {
    return playbackMode;
  }

  public void setPlaybackMode(PlaybackMode playbackMode) {
    this.playbackMode = playbackMode;
  }

  public UUID getLastProcessedMessage() {
    return lastProcessedMessage;
  }

  public void setLastProcessedMessage(UUID lastProcessedMessage) {
    this.lastProcessedMessage = lastProcessedMessage;
  }

  public long getEventCount() {
    return eventCount;
  }

  public void setEventCount(long eventCount) {
    this.eventCount = eventCount;
  }

  public List<Message> getRecentEvents() {
    return recentEvents;
  }

  public void setRecentEvents(List<Message> recentEvents) {
    this.recentEvents = recentEvents;
  }

  public int getMaxRecentEvents() {
    return maxRecentEvents;
  }

  public void setMaxRecentEvents(int maxRecentEvents) {
    this.maxRecentEvents = maxRecentEvents;
  }

  public T getState() {
    return state;
  }

  public void setState(T state) {
    this.state = state;
  }

  public UUID getSerializedAtMessage() {
    return serializedAtMessage;
  }

  public void setSerializedAtMessage(UUID serializedAtMessage) {
    this.serializedAtMessage = serializedAtMessage;
  }

  public long getEventsSinceLastSerialization() {
    return eventsSinceLastSerialization;
  }

  public void setEventsSinceLastSerialization(long eventsSinceLastSerialization) {
    this.eventsSinceLastSerialization = eventsSinceLastSerialization;
  }

  public MessageConsumptionMode getMessageConsumptionMode() {
    return messageConsumptionMode;
  }

  public void setMessageConsumptionMode(MessageConsumptionMode messageConsumptionMode) {
    this.messageConsumptionMode = messageConsumptionMode;
  }

  public EventPoller getEventPoller() {
    return eventPoller;
  }

  public void setEventPoller(EventPoller eventPoller) {
    this.eventPoller = eventPoller;
  }

  public EventPublisher getPublisher() {
    return publisher;
  }

  public void setPublisher(EventPublisher publisher) {
    this.publisher = publisher;
  }
}
