/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.MessageHeader;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.wrappers.BasicMessage;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class EventPublisherImpl implements EventPublisher {
  private static final Logger logger = Logger.getLogger(EventPublisherImpl.class.getName());
  private final HalPublisher halPublisher;

  private final State state;

  private final String defaultChannel;
  private final String defaultTopic;

  public EventPublisherImpl(State state, HalPublisher halPublisher,
                            String defaultChannel, String defaultTopic) {
    this.state = state;
    this.halPublisher = halPublisher;
    this.defaultChannel = defaultChannel;
    this.defaultTopic = defaultTopic;
  }

  private Date getTime() {
    return new Date();
  }

  @Override
  public void publish(Message event) {
    if (state.getPlaybackMode() != PlaybackMode.LIVE) {
      throw new IllegalStateException(String.format("Message publication is only allowed when the playback mode is %s. " +
              "An attempt was made to publish a message when the playback mode was %s (this is a bug).",
          PlaybackMode.LIVE.name(),
          state.getPlaybackMode()
      ));
    }

    halPublisher.publish(event);
  }

  @Override
  public void publishAll(List<Message> events) {
    if (state.getPlaybackMode() != PlaybackMode.LIVE) {
      throw new IllegalStateException(String.format("Message publication is only allowed when the playback mode is %s. " +
              "An attempt was made to publish a message when the playback mode was %s (this is a bug).",
          PlaybackMode.LIVE.name(),
          state.getPlaybackMode()
      ));
    }

    halPublisher.publishAll(events);
  }

  public void emitPayload(UUID inResponseTo, Object payload) {
    emitPayload(defaultChannel, defaultTopic, inResponseTo, payload);
  }

  public void emitPayload(Object payload) {
    emitPayload(null, payload);
  }

  @Override
  public void emitPayload(String topicOverride, UUID inResponseTo, Object payload) {
    emitPayload(defaultChannel, topicOverride, inResponseTo, payload);
  }

  @Override
  public void emitPayload(String channelOverride, String topicOverride, UUID inResponseTo, Object payload) {
    if (state.getPlaybackMode() != PlaybackMode.LIVE) {
      throw new IllegalStateException(String.format("Message publication is only allowed when the playback mode is %s. " +
              "An attempt was made to publish a message when the playback mode was %s (this is a bug).",
          PlaybackMode.LIVE.name(),
          state.getPlaybackMode()
      ));
    }

    MessageHeader header = new MessageHeader();
    header.setParentId(inResponseTo);
    header.setDate(getTime());

    //header.setPublisherId(halogenPublisherId);  // Set by halPublisher
    header.setChannel(channelOverride);
    header.setTopic(topicOverride);
    header.setType(null); // Currently not used

    Message eventMessage = new BasicMessage();
    eventMessage.setHeader(header);
    eventMessage.setContent(payload);

    //halPublisher.publish(EventMessage.toStringMessage(gson, eventMessage));
    halPublisher.publish(eventMessage);
  }
}
