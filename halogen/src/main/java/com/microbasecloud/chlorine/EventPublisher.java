/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public interface EventPublisher {
  void publish(Message event);
  void publishAll(List<Message> events);

  void emitPayload(Object payload);
  void emitPayload(UUID inResponseTo, Object payload);

  /**
   * Emits a payload, allowing you to override the default topic name.
   *
   * @param topicOverride
   * @param inResponseTo
   * @param payload
   */
  void emitPayload(String topicOverride, UUID inResponseTo, Object payload);

  /**
   * Emits a payload, allowing you to override the default channel and topic names.
   *
   * @param channelOverride
   * @param topicOverride
   * @param inResponseTo
   * @param payload
   */
  void emitPayload(String channelOverride, String topicOverride, UUID inResponseTo, Object payload);
}
