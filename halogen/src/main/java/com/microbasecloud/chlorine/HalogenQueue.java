/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;


import java.util.Deque;

/**
 * @author Keith Flanagan
 */
public interface HalogenQueue<T> {

  /**
   * Manually contact the messaging server and pull new items that are not already in our local queue.
   * This is not necessary if you use <code>poll</code>, since <code>poll</code> will automatically
   * repopulate the local queue if it is empty
   */
  void repopulateQueue();

  Message<T> peek();

  Message<T> poll(boolean block);
  Message<T> poll(long maxWaitMs);

  //void addToQueue(Collection<StringMessage> messages);


//  String getHalogenChannel();
//  void setHalogenChannel(String halogenChannel);
//
//  String getHalogenTopic();
//  void setHalogenTopic(String halogenTopic);
//
  int getMinPollSize();
  void setMinPollSize(int minPollSize);

  int getMaxPollSize();
  void setMaxPollSize(int maxPollSize);

//  UUID getMessagePointer();
//  void setMessagePointer(UUID messagePointer);

//  HalogenStorage<StringMessage> getClient();
//  void setClient(HalogenStorage<StringMessage> client);

  Deque<Message<T>> getLocalQueue();
  void setLocalQueue(Deque<Message<T>> localQueue);
}
