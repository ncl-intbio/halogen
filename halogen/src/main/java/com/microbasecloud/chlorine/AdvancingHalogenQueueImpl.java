/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;

import java.util.*;
import java.util.logging.Logger;

/**
 * Stores a pool of messages locally, retrieved from the remote Halogen server.
 * This Halogen queue implementation relies on the server-side message pointer when polling for a given subscriber.
 * Therefore, this queue has the following semantics:
 *   - Any messages received by a particular instance are guaranteed not to be seen by other queue instances
 *     that happen to be using the same subscriber ID.
 *   - Therefore, you must take care not to "lose" messages somehow (e.g., by terminating a client before enacting
 *     all of the received messages, or otherwise storing them).
 *   - You can use this queue implementation to implement things like a job queue, with each worker node sharing
 *     the same subscriber ID.
 *
 * @author Keith Flanagan
 */
public class AdvancingHalogenQueueImpl<T> extends AbstractHalogenQueue<T> implements AdvancingHalogenQueue<T> {
  private static final Logger logger = Logger.getLogger(AdvancingHalogenQueueImpl.class.getName());

  public AdvancingHalogenQueueImpl(HalSubscriber<T> client, int maxPollSize, int maxQueueSize) {
    super(client, maxPollSize, maxQueueSize);
  }

  /**
   * Poll messaging server and pull new items that are not already in our local queue
   */
  @Override
  public void repopulateQueue() {
    if (!localQueue.isEmpty()) {
      return;
    }

    final int pollSize = Math.min(maxPollSize, remainingCapacity());
//    if (pollSize < minPollSize) {
//      // The local queue is nowhere near empty - it's not worth a round-trip yet.
//      return;
//    }

    List<Message<T>> messages = client.consume(pollSize, true);
    addToQueue(messages);
  }

  private void addToQueue(Collection<Message<T>> messages) {
    for (Message<T> message : messages) {
      if (localQueue.size() >= maxQueueSize) {
        logger.info("It looks like an attempt was made to over-fill the local queue. This is a bug and you will lose messages this way!");
        break;
      }

      boolean added = localQueue.offer(message);
      if (!added) {
        logger.info("It looks like the local queue was unexpectedly full. This is a bug. " +
            "However, we should fail-safe here, so probably no harm done!");
        break;
      }
    }
  }

  @Override
  public UUID getRemoteMessagePointer() {
    throw new UnsupportedOperationException("Not yet implemented!");
  }

  @Override
  public void setRemoteMessagePointer(UUID messagePointer) {
    client.setMessagePointer(messagePointer);
    // We now need to clear the local queue to ensure consistency
    localQueue.clear();
  }
}
