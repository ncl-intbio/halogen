/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.Deque;
import java.util.LinkedList;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
abstract public class AbstractHalogenQueue<T> implements HalogenQueue<T> {
  private static final Logger logger = Logger.getLogger(AbstractHalogenQueue.class.getName());
  private static final long DEFAULT_REPOPULATION_EVERY = 3000L;

  protected HalSubscriber<T> client;

  protected int minPollSize;
  protected int maxPollSize;
  protected Deque<Message<T>> localQueue;

  protected int maxQueueSize;

  private long nextRepopulateAllowedAt; // Controls time between repopulateQueue() calls when withing tight loops
  private long repopulationAllowedEvery = DEFAULT_REPOPULATION_EVERY;

  public AbstractHalogenQueue(HalSubscriber<T> client, int maxPollSize, int maxQueueSize) {
    this.client = client;
    this.maxPollSize = maxPollSize;
    this.maxQueueSize = maxQueueSize;
    this.localQueue = new LinkedList<>();

    this.minPollSize = Math.max(1, maxPollSize / 2);

    if (maxPollSize < minPollSize) {
      throw new IllegalArgumentException("Max poll size should be >= min poll size!");
    }
    if (maxPollSize > maxQueueSize) {
      throw new IllegalArgumentException("Max poll size should be <= max queue size!");
    }
  }


  protected int remainingCapacity() {
    int remainingCapacity = maxQueueSize - localQueue.size();
    if (remainingCapacity < 0) {
      remainingCapacity = 0;
    }
    return remainingCapacity;
  }

  private void repopulateQueueFromWithinLoop() {
    if (System.currentTimeMillis() < nextRepopulateAllowedAt) {
      return; // Not yet allowed to call 'repopulate' again
    }
    repopulateQueue();
    nextRepopulateAllowedAt = System.currentTimeMillis() + repopulationAllowedEvery;
  }

  @Override
  public Message<T> peek() {
    // Provide an opportunity to re-stock the local queue, if required.
    repopulateQueue(); // This only calls the server if the local queue is sufficiently empty.

    return localQueue.peek();
  }

  @Override
  public Message<T> poll(boolean block) {
    // Provide an opportunity to re-stock the local queue, if required.
    repopulateQueue(); // This only calls the server if the local queue is sufficiently empty.

    if (!block) {
      return localQueue.poll(); // Returns an element, or null if local queue is empty
    }

    while (localQueue.isEmpty()) {
      ThreadUtils.sleep(repopulationAllowedEvery + 100);
      repopulateQueueFromWithinLoop();
    }
    return localQueue.poll();
  }

  @Override
  public Message<T> poll(long maxWaitMs) {
    if (maxWaitMs < 0) {
      throw new IllegalArgumentException("Wait time must be >= 0");
    }

    long endTime = System.currentTimeMillis() + maxWaitMs;

    // Provide an opportunity to re-stock the local queue, if required.
    repopulateQueue(); // This only calls the server if the local queue is sufficiently empty.

    while (localQueue.isEmpty() && System.currentTimeMillis() < endTime) {
      ThreadUtils.sleep(25L);
      repopulateQueueFromWithinLoop();
    }

    return localQueue.poll();
  }

  public static Logger getLogger() {
    return logger;
  }

  public HalSubscriber<T> getClient() {
    return client;
  }

  public void setClient(HalSubscriber<T> client) {
    this.client = client;
  }

  @Override
  public int getMinPollSize() {
    return minPollSize;
  }

  @Override
  public void setMinPollSize(int minPollSize) {
    this.minPollSize = minPollSize;
  }

  @Override
  public int getMaxPollSize() {
    return maxPollSize;
  }

  @Override
  public void setMaxPollSize(int maxPollSize) {
    this.maxPollSize = maxPollSize;
  }

  @Override
  public Deque<Message<T>> getLocalQueue() {
    return localQueue;
  }

  @Override
  public void setLocalQueue(Deque<Message<T>> localQueue) {
    this.localQueue = localQueue;
  }

  public int getMaxQueueSize() {
    return maxQueueSize;
  }

  public void setMaxQueueSize(int maxQueueSize) {
    this.maxQueueSize = maxQueueSize;
  }
}
