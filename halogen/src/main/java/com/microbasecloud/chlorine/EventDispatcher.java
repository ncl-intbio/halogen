/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class EventDispatcher implements EventProcessor {
  private static final Logger logger = Logger.getLogger(EventDispatcher.class.getName());

  private List<EventProcessor> listeners = new LinkedList<>();

  @Override
  public void onIncomingMessage(State state, Message event) {
    if (event == null) {
      logger.info("Incoming message was NULL!");
      throw new RuntimeException("Incoming message was NULL!");
    }

    if (event.getContent() == null) {
      logger.info("Incoming message content was NULL!");
      throw new RuntimeException("Incoming message content was NULL!");
    }

    for (EventProcessor listener : listeners) {
      if (listener.canProcess(event)) {
        try {
          listener.onIncomingMessage(state, event);
        } catch (Exception e) {
          logger.info("Listener failed!");
          e.printStackTrace();
        }
      }
    }

    state.addCompletedMessage(state, event);
  }

  public void addListener(EventProcessor listener) {
    listeners.add(listener);
  }

  @Override
  public boolean canProcess(Message event) {
    return true;
  }
}
