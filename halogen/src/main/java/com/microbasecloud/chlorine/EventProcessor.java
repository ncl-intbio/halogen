/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.chlorine;

import com.microbasecloud.halogen.Message;

/**
 * @author Keith Flanagan
 */
public interface EventProcessor<S, E> {
  /**
   * Return true if this event processor is capable of processing the message, otherwise, false.
   * @param event
   * @return
   */
  boolean canProcess(Message event);
  void onIncomingMessage(State<S> state, Message<E> event);
}
