/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.chlorine;

/**
 * @author Keith Flanagan
 */
public interface EventPoller {
  /**
   * Used on bootup to play back the effects of any existing events that occurred since the last state change.
   *
   * It is not permitted for any <code>EventProcessor</code> to publish new events during this process.
   *
   * It is not permitted to execute <code>replayEvents</code> more than once, or after normal event processing
   * has resumed.
   */
  void replayEvents();

  /**
   * Consumes the next message from (one of the) message queue(s).
   *
   * @return true if an action was taken (e.g. a message was processed).
   *         false if no action was taken (either because there were no messages, or because it would have been illegal)
   */
  boolean consumeNext();

  boolean isReplayCalled();

  long getConsumeCount();
}
