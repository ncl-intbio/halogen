/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.fluorine;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.util.TimeService;
import com.scalesinformatics.dbpool.DistributedJdbcPool;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * An implementation of <code>ObjectStore</code> that keeps only the latest version of a specified document
 *
 * @author Keith Flanagan
 */
public class ObjectStoreSingleVersionJsonSqlImpl<T extends VersionableObject> implements ObjectStore<T> {
  private static final Logger logger = Logger.getLogger(ObjectStoreSingleVersionJsonSqlImpl.class.getName());

  protected final DistributedJdbcPool connFact;
  protected final String table;
  protected final Gson gson;
  protected final TypeToken<T> beanType;
  protected final TimeService timeService;

  public ObjectStoreSingleVersionJsonSqlImpl(
          DistributedJdbcPool connFact, String table,
          Gson gson, TypeToken<T> beanType,
          TimeService timeService) {
    this.connFact = connFact;
    this.table = table;
    this.gson = gson;
    this.beanType = beanType;
    this.timeService = timeService;
  }

  @Override
  public T getByUid(UUID uid) throws ObjectStoreException, ObjectNotFoundException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = prepareGetByUid(conn)) {
      stmt.setObject(1, uid);
      T result = executeAndParse(stmt);
      if (result == null) {
        throw new ObjectNotFoundException(uid, -1);
      }
      return result;
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  private PreparedStatement prepareGetByUid(Connection conn) throws SQLException {
    return conn.prepareStatement("SELECT data FROM "+table+" WHERE uid = ? ORDER BY version DESC LIMIT 1");
  }

  private T executeAndParse(PreparedStatement stmt) throws SQLException, ObjectStoreException {
    try (ResultSet rs = stmt.executeQuery()) {
      if (!rs.next()) {
        return null;
      } else {
        //return deserialize((PGobject) rs.getObject("data"));
        return deserialize(rs.getString("data"));
      }
    }
  }

  @Override
  public T getByUid(UUID uid, long version) throws ObjectStoreException, ObjectNotFoundException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(
             "SELECT data FROM "+table+" WHERE uid = ? AND version = ?")) {
      stmt.setObject(1, uid);
      stmt.setLong(2, version);
      T result = executeAndParse(stmt);
      if (result == null) {
        throw new ObjectNotFoundException(uid, version);
      }
      return result;
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  @Override
  public boolean existsByUid(UUID uid) throws ObjectStoreException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = prepareGetByUid(conn)) {
      stmt.setObject(1, uid);
      return executeAndParse(stmt) != null;
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  @Override
  public boolean deleteByUid(UUID uid) throws ObjectStoreException {
    try (Connection conn = connFact.getWritableConnection()) {
      boolean deleted = deleteByUid(conn, uid) > 0;
      conn.commit(); // TODO wrap this in a 'transactional' lambda
      return deleted;
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  private int deleteByUid(Connection conn, UUID uid) throws SQLException {
    try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM "+table+" WHERE uid = ?")) {
      stmt.setObject(1, uid);
      return stmt.executeUpdate();
    }
  }

  @Override
  public long getLastVersionOf(UUID uid) throws ObjectStoreException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = prepareLastVersionOf(conn)) {
      return _getLastVersionOf(stmt, uid);
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  private PreparedStatement prepareLastVersionOf(Connection conn) throws SQLException {
    return conn.prepareStatement("SELECT version FROM "+table+" WHERE uid = ? ORDER BY version DESC LIMIT 1");
  }

  private long _getLastVersionOf(PreparedStatement stmt, UUID uid) throws SQLException {
    stmt.setObject(1, uid);
    try (ResultSet rs = stmt.executeQuery()) {
      if (!rs.next()) {
        return -1;
      } else {
        return rs.getLong("version");
      }
    }
  }

  private PreparedStatement prepareLastMetaDataOf(Connection conn) throws SQLException {
    return conn.prepareStatement("SELECT uid, version, updated_at, update_id, type FROM " + table
        + " WHERE uid = ? ORDER BY version DESC LIMIT 1");

  }


  private ObjectMetadata _getLastMetaDataByUid(PreparedStatement stmt, UUID uid) throws SQLException {
    stmt.setObject(1, uid);
    try (ResultSet rs = stmt.executeQuery()) {
      if (!rs.next()) {
        return new ObjectMetadata(uid, -1, this.timeService.getNow(), null, null);
      } else {
        return new ObjectMetadata(
            (UUID) rs.getObject("uid"), rs.getLong("version"), rs.getLong("updated_at"),
            (UUID) rs.getObject("update_id"), rs.getString("type"));
      }
    }
  }

  @Override
  public ObjectMetadata getObjectMetaData(UUID uid) throws ObjectStoreException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = prepareLastMetaDataOf(conn)) {
      return _getLastMetaDataByUid(stmt, uid);
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  protected List<ObjectMetadata> rsToMetaDataList(PreparedStatement stmt) throws ObjectStoreException {
    try (ResultSet rs = stmt.executeQuery()) {
      List<ObjectMetadata> history = new ArrayList<>();
      while (rs.next()) {
        history.add(new ObjectMetadata(
            (UUID) rs.getObject("uid"), rs.getLong("version"), rs.getLong("updated_at"),
            (UUID) rs.getObject("update_id"), rs.getString("type")));
      }
      return history;
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to perform database operation", e);
    }
  }

  @Override
  public List<ObjectMetadata> getVersionHistoryFor(UUID uid) throws ObjectStoreException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement("SELECT * FROM "+table+" WHERE uid = ? ORDER BY version")) {
      stmt.setObject(1, uid);
      List<ObjectMetadata> history = rsToMetaDataList(stmt);
      return history;
    } catch (SQLException e) {
      throw new ObjectStoreException("Failed to query the database", e);
    }
  }

  @Override
  public void put(T object) throws ObjectStoreException, VersionConflictException {
    try (Connection conn = connFact.getWritableConnection();
         PreparedStatement insertStmt = prepareInsert(conn);
         PreparedStatement lastVersionStmt = prepareLastMetaDataOf(conn)) {
      if (object.getUid() == null) {
        throw new ObjectStoreException("Stored objects must have a non-null UID field.");
      }

      ObjectMetadata lastRowData = _getLastMetaDataByUid(lastVersionStmt, object.getUid());
      // The new version must be exactly one more than the previous version
      long expectedVersion = lastRowData.getVersion() + 1;
      if (object.getVersion() != expectedVersion) {
        throw new VersionConflictException("Attempting to store version: "+object.getVersion()+
            ". However, the last stored version was: "+lastRowData.getVersion()+", which was not the expected: "+expectedVersion);
      }

      deleteByUid(conn, object.getUid()); // Delete the old version, if it exists
      store(insertStmt, object);
      insertStmt.execute();
      conn.commit();
    } catch (SQLException | IOException | RuntimeException e) {
      throw new ObjectStoreException("Failed to store object: " + object, e);
    }
  }

  private PreparedStatement prepareInsert(Connection conn) throws SQLException {
    return conn.prepareStatement(
        "INSERT INTO "+table+" (" +
            "uid, version, updated_at, updated_at_date, update_id, type, data" +
            ") VALUES (?,?,?,?,?,?,?)");
  }

  private void store(PreparedStatement insert, T object)
      throws SQLException, IOException {
    int col = 1;

    long now = this.timeService.getNow();
    insert.setObject(col++, object.getUid());
    insert.setLong(col++, object.getVersion());
    insert.setLong(col++, now);
    insert.setTimestamp(col++, new Timestamp(now));
    insert.setObject(col++, object.getUpdateId());
    insert.setString(col++, object.getClass().getName());

    // Workaround for no JDBC JSON type
//    PGobject jsonObject = new PGobject();
//    jsonObject.setType("jsonb");
//    jsonObject.setValue(gson.toJson(object));
//    insert.setObject(col++, jsonObject);
    insert.setString(col++, gson.toJson(object));
  }

  @Override
  public List<T> listLatestEntriesForAllItems() throws ObjectStoreException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement select = conn.prepareStatement(
             "SELECT * FROM " + table)) {
      List<T> entries = new ArrayList<>();
      try (ResultSet rs = select.executeQuery()) {
        while (rs.next()) {
          entries.add(rsToObject(rs));
        }
      }
      return entries;
    } catch (Exception e) {
      throw new ObjectStoreException("Failed to perform query.", e);
    }
  }

  protected T rsToObject(ResultSet rs) throws SQLException, ObjectStoreException {
    //return deserialize((PGobject) rs.getObject("data"));
    return deserialize(rs.getString("data"));
  }

  protected T deserialize(String jsonText) throws ObjectStoreException {
//    try {
//      return gson.fromJson(jsonObject.getValue(), beanType.getType());
//    } catch (Exception e) {
//      logger.info("Failed to deserialize text: "+jsonObject.getValue());
//      throw new ObjectStoreException(
//          "Failed to deserialize object to type: "+beanType.toString(), e);
//    }
    try {
      return gson.fromJson(jsonText, beanType.getType());
    } catch (Exception e) {
      logger.info("Failed to deserialize text: "+jsonText);
      throw new ObjectStoreException(
          "Failed to deserialize object to type: "+beanType.toString(), e);
    }
  }
}
