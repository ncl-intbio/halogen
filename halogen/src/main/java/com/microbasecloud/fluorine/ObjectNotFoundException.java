package com.microbasecloud.fluorine;

import java.util.UUID;

/**
 * Signals that no object could be found for the provided parameters.
 */
public class ObjectNotFoundException extends Exception {
    private UUID uid;
    private long version;

    private static final String messageFormatUidAndVersion =
            "No object could be found with UID %s and version %s.";


    public ObjectNotFoundException(UUID uid, long version) {
        this (String.format(messageFormatUidAndVersion, uid, version), uid, version);
    }

    public ObjectNotFoundException(String message, UUID uid, long version) {
        super(message);
        this.uid = uid;
        this.version = version;
    }

    public ObjectNotFoundException(String message, Throwable cause, UUID uid, long version) {
        super(message, cause);
        this.uid = uid;
        this.version = version;
    }

    public ObjectNotFoundException(Throwable cause, UUID uid, long version) {
        super(String.format(messageFormatUidAndVersion, uid, version), cause);
        this.uid = uid;
        this.version = version;
    }

    public ObjectNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, UUID uid, long version) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.uid = uid;
        this.version = version;
    }

    public String makeErrorMessage()
    {
        return String.format(messageFormatUidAndVersion, uid.toString(), version);
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
