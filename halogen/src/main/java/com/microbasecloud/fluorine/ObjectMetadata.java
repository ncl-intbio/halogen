package com.microbasecloud.fluorine;

import java.util.UUID;

/**
 * Created by jimi on 17/11/16.
 */
public class ObjectMetadata {
  //"uid, version, timestamp, date, update_id, type, data" +
  private final UUID uid;
  private final long version;
  private final long timestamp;
  private final UUID updateId;
  private final String dataType;

  public ObjectMetadata(UUID uid, long version, long timestamp, UUID updateId, String dataType) {
    this.uid = uid;
    this.version = version;
    this.timestamp = timestamp;
    this.updateId = updateId;
    this.dataType = dataType;
  }

  public UUID getUid() {
    return uid;
  }

  public long getVersion() {
    return version;
  }

  public UUID getUpdateId() {
    return updateId;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public String getDataType() {
    return dataType;
  }
}
