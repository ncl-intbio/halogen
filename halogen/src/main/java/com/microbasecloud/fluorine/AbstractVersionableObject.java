/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.fluorine;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public abstract class AbstractVersionableObject implements VersionableObject {
  private UUID uid;       // The ID of this object
  private long version;    // The version number (i.e. a count of how many updates there have been)

  private UUID updateId; // Optional field that identifies the last update (e.g., where it came from)
  private Date updatedAt; // Date stamp of this version [optional]


  public AbstractVersionableObject() {
    this(UUID.randomUUID(), 0, new Date());
  }

  public AbstractVersionableObject(UUID uid, int version) {
    this(uid, version, new Date());
  }

  public AbstractVersionableObject(UUID uid, int version, Date updatedAt) {
    this.uid = uid;
    this.version = version;
    this.updateId = UUID.randomUUID();
    this.updatedAt = updatedAt;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AbstractVersionableObject that = (AbstractVersionableObject) o;
    return version == that.version &&
        Objects.equals(uid, that.uid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uid, version);
  }

  @Override
  public void incrementVersion() {
    version++;
    updateId = UUID.randomUUID();
    updatedAt = new Date();
  }

  @Override
  public UUID getUid() {
    return uid;
  }

  @Override
  public void setUid(UUID uid) {
    this.uid = uid;
  }

  @Override
  public long getVersion() {
    return version;
  }

  @Override
  public void setVersion(long version) {
    this.version = version;
  }

  @Override
  public UUID getUpdateId() {
    return updateId;
  }

  @Override
  public void setUpdateId(UUID updateId) {
    this.updateId = updateId;
  }

  @Override
  public Date getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }
}
