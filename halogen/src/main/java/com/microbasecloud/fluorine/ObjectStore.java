/*
 * Copyright 2016 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.fluorine;

import java.util.List;
import java.util.UUID;

/**
 * Defines a storage mechanism for a set of <code>VersionableObject</code> instances of type <code>T</code>.
 * Each <code>VersionableObject</code> is a data bean that contains a UID, and version number, in addition to
 * user-defined fields.
 *
 * Object versions are integers. The initial version of an object must be zero. Inserting a new version of an object
 * must increment the previous version number by exactly one.
 *
 * @author Keith Flanagan
 */
public interface ObjectStore<T extends VersionableObject> {
  /**
   * Returns the latest version of the specified object.
   *
   * @param uid the ID of the object to return
   * @return the latest version of the object identified by <code>uid</code>.
   * @throws ObjectStoreException if an unexpected data store error occurred
   * @throws ObjectNotFoundException if no object matching the <code>uid</code> could be found
   */
  T getByUid(UUID uid) throws ObjectStoreException, ObjectNotFoundException;

  /**
   * Returns a specific version of the object
   *
   * @param uid the object ID
   * @param version the object version
   * @return the specified <code>version</code> of the object identified by <code>uid</code>.
   * @throws ObjectStoreException if an unexpected data store error occurred
   * @throws ObjectNotFoundException if no object matching the <code>uid</code> could be found
   */
  T getByUid(UUID uid, long version) throws ObjectStoreException, ObjectNotFoundException;

  /**
   * Returns whether an object is present in the data store.
   *
   * @param uid the object ID
   * @return true if the object exists, otherwise false.
   * @throws ObjectStoreException if an unexpected data store error occurred
   */
  boolean existsByUid(UUID uid) throws ObjectStoreException;

  /**
   * Deletes all entries of an object with the specified <code>uid</code>.
   *
   * @param uid the object ID
   * @throws ObjectStoreException if an unexpected data store error occurred
   * @return Returns <code>true</code> an entry with UID <code>uid</code> was deleted successfully.
   * Returns <code>false</code> if the data store operation succeeded, but no entries with UID
   * <code>uid</code> were found.
   * @throws ObjectStoreException
   */
  boolean deleteByUid(UUID uid) throws ObjectStoreException;

  /**
   * Returns the latest version number of an object
   *
   * @param uid the object ID
   * @return the latest version number of the specified object identified <code>uid</code>.
   * Returns -1 if no version of the specified object exists in the data store.
   * @throws ObjectStoreException if an unexpected data store error occurred
   */
  long getLastVersionOf(UUID uid) throws ObjectStoreException;

  /**
   * Returns metadata about the last update to a specified object
   * @param uid the object ID
   * @return a <code>ObjectMetadata</code> object containing various information about the last update to the
   * object identified by <code>uid</code>.
   * @throws ObjectStoreException
   */
  ObjectMetadata getObjectMetaData(UUID uid) throws ObjectStoreException;

  /**
   * Returns information about the entire version history of an object
   * @param uid the object ID
   * @return historical metadata about updates to the object identified by <code>uid</code>.
   * @throws ObjectStoreException
   */
  List<ObjectMetadata> getVersionHistoryFor(UUID uid) throws ObjectStoreException;

  /**
   * Stores a new version of an object to the database.
   *
   * @param object the <code>VersionableObject</code> that is to be stored.
   * @throws ObjectStoreException if an unexpected data store error occurred
   * @throws VersionConflictException if the version number in <code>object</code> was not as expected.
   * Version numbers must be '0' for new objects. For existing objects, the version number must be exactly
   * <code>existing_version + 1</code>.
   */
  void put(T object) throws ObjectStoreException, VersionConflictException;

  /**
   * Returns the latest version all object instances.
   *
   * Caution: depending on the size or number of objects stored, this operation may require a large amount of RAM.
   * TODO we need to add a 'paged' version of this call in future.
   *
   * @return a list of the latest object versions. i.e., for each object UID, returns object instance with
   * the largest version number.
   * @throws ObjectStoreException
   */
  List<T> listLatestEntriesForAllItems() throws ObjectStoreException;

}
