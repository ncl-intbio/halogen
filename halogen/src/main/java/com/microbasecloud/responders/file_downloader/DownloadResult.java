/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.responders.file_downloader;

import com.microbasecloud.chlorine.Fact;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class DownloadResult implements Fact {
  private UUID fileUniqueId;
  private String sourceUrl;

  private boolean success;
  private String message;

  public DownloadResult() {
  }

  public DownloadResult(DownloadRequest request, boolean success, String message) {
    this(request.getFileUniqueId(), request.getSourceUrl(), success, message);
  }

  public DownloadResult(UUID fileUniqueId, String sourceUrl, boolean success, String message) {
    this.fileUniqueId = fileUniqueId;
    this.sourceUrl = sourceUrl;
    this.success = success;
    this.message = message;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public UUID getFileUniqueId() {
    return fileUniqueId;
  }

  public void setFileUniqueId(UUID fileUniqueId) {
    this.fileUniqueId = fileUniqueId;
  }
}
