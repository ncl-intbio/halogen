/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.responders.file_downloader;

import com.microbasecloud.chlorine.Command;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class DownloadRequest implements Command {
  private static final int DEFAULT_MAX_RETRIES = 20;
  private static final long DEFAULT_TIME_BETWEEN_RETRIES = 20000L;


  /**
   * The UUID that the requested download will be known as on completion.
   */
  private UUID fileUniqueId;

  /**
   * The source location to obtain a file from.
   */
  private String sourceUrl;

  private String successTopic;
  private String failureTopic;

  private int maxRetries;            // Max number of times we try to re-download a file before failing
  private long timeBetweenRetriesMs; // Milliseconds to wait between download retries

  public DownloadRequest() {
  }

  public DownloadRequest(String sourceUrl, String successTopic, String failureTopic) {
    this.sourceUrl = sourceUrl;
    this.successTopic = successTopic;
    this.failureTopic = failureTopic;
    this.maxRetries =  DEFAULT_MAX_RETRIES;
    this.timeBetweenRetriesMs = DEFAULT_TIME_BETWEEN_RETRIES;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  public String getSuccessTopic() {
    return successTopic;
  }

  public void setSuccessTopic(String successTopic) {
    this.successTopic = successTopic;
  }

  public String getFailureTopic() {
    return failureTopic;
  }

  public void setFailureTopic(String failureTopic) {
    this.failureTopic = failureTopic;
  }

  public UUID getFileUniqueId() {
    return fileUniqueId;
  }

  public void setFileUniqueId(UUID fileUniqueId) {
    this.fileUniqueId = fileUniqueId;
  }

  public int getMaxRetries() {
    return maxRetries;
  }

  public void setMaxRetries(int maxRetries) {
    this.maxRetries = maxRetries;
  }

  public long getTimeBetweenRetriesMs() {
    return timeBetweenRetriesMs;
  }

  public void setTimeBetweenRetriesMs(long timeBetweenRetriesMs) {
    this.timeBetweenRetriesMs = timeBetweenRetriesMs;
  }
}
