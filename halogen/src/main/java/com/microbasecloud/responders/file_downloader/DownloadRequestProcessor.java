/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.responders.file_downloader;

import com.microbasecloud.chlorine.EventProcessor;
import com.microbasecloud.chlorine.PlaybackMode;
import com.microbasecloud.chlorine.State;
import com.microbasecloud.halogen.Message;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class DownloadRequestProcessor<S> implements EventProcessor<S, DownloadRequest> {
  private static final Logger logger = Logger.getLogger(DownloadRequestProcessor.class.getName());

  private File storageDirectory;

  public DownloadRequestProcessor(File storageDirectory) {
    this.storageDirectory = storageDirectory;
  }

  @Override
  public boolean canProcess(Message event) {
    return event.getContent() instanceof DownloadRequest;
  }

  @Override
  public void onIncomingMessage(State<S> state, Message<DownloadRequest> event) {
    if (state.getPlaybackMode() != PlaybackMode.LIVE) {
      return;
    }

    DownloadRequest content = event.getContent();

    final URL url;
    try {
      url = new URL(content.getSourceUrl());
    } catch (Exception e) {
      e.printStackTrace();
      state.getPublisher().emitPayload(content.getFailureTopic(), event.getHeader().getUid(),
          new DownloadResult(content, false, "Failed to parse URL: "+content.getSourceUrl()));
      return;
    }

    File outputFile = new File(storageDirectory, content.getFileUniqueId().toString());
    if (outputFile.exists()) {
      state.getPublisher().emitPayload(content.getFailureTopic(), event.getHeader().getUid(),
          new DownloadResult(content, false, String.format("A file with UID %s already exists!", outputFile.getName())));
      return;
    }

    boolean success = false;
    Exception lastException = null;
    for (int i=0; i<content.getMaxRetries(); i++) {
      try (BufferedInputStream is = new BufferedInputStream(url.openStream());
           FileOutputStream os = new FileOutputStream(new File(storageDirectory, content.getFileUniqueId().toString()))) {
        byte[] buffer = new byte[4096];
        for (int read = is.read(buffer); read >= 0; read = is.read(buffer)) {
          os.write(buffer, 0, read);
        }
        os.flush(); // Just in case...
        logger.info(String.format("File downloaded successfully: %s", content.getSuccessTopic()));
        success = true;
        break;
      } catch (Exception e) {
        lastException = e;

        logger.info(String.format("Failed to download remote file %s. This is try %s of %s. " +
                "See the following stack trace for more details. Going to wait %sms before trying again.",
            content.getSourceUrl(), i, content.getMaxRetries(), content.getTimeBetweenRetriesMs()));
        e.printStackTrace();
        ThreadUtils.sleep(content.getTimeBetweenRetriesMs());
        success = false;
      }
    }

    if (success) {
      state.getPublisher().emitPayload(content.getSuccessTopic(), event.getHeader().getUid(),
          new DownloadResult(content, true, String.format("Download complete: %s", outputFile.getName())));
    } else {
      state.getPublisher().emitPayload(content.getFailureTopic(), event.getHeader().getUid(),
          new DownloadResult(content, false,
              String.format("Failed to download or write the file %s because: %s",
                  outputFile.getName(), lastException != null ? lastException.getMessage() : "lastException was null!")));
    }
  }
}
