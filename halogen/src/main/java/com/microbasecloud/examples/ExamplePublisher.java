/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.examples;

import com.google.gson.Gson;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.MessageHeader;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.publisher.HalPublisherWsClient;
import com.microbasecloud.halogen.wrappers.BasicMessage;
import com.microbasecloud.halogen.wrappers.HalPublisherWrapper;
import com.microbasecloud.util.GsonFactory;

import java.util.*;

/**
 * @author Keith Flanagan
 */
public class ExamplePublisher {
  public static List<String> makeChannels(int num) {
    List<String> channels = new ArrayList<>();
    for (int i=0; i<num; i++) {
      channels.add("Channel_"+i);
    }
    return channels;
  }

  public static List<String> makeTopics(int num) {
    List<String> topics = new ArrayList<>();
    for (int i=0; i<num; i++) {
      topics.add("Topic_"+i);
    }
    return topics;
  }

  public static List<String> makeTypes(int num) {
    List<String> types = new ArrayList<>();
    for (int i=0; i<num; i++) {
      types.add("Type_"+i);
    }
    return types;
  }

  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Usage:");
      System.out.println("<halogen_url> <num_messages>");
      System.exit(1);
    }

    String halogenUrl = args[0];
    int numMessages = Integer.parseInt(args[1]);

    HalAuth publisherCreds = new HalAuth(new UUID(0L, 0L), new UUID(0L, 0L));

    List<String> channels = makeChannels(5);
    List<String> topics = makeTopics(5);
    List<String> types = makeTypes(5);

    Gson gson = GsonFactory.make();

    HalPublisher publisher = new HalPublisherWrapper(gson, new HalPublisherWsClient(gson, halogenUrl, publisherCreds));

    Random rnd = new Random();
    for (int m=0; m<numMessages; m++) {
      String content = "Message: "+m;

      Message<String> message = makeMessage(
          channels.get(rnd.nextInt(channels.size())),
          topics.get(rnd.nextInt(topics.size())),
          types.get(rnd.nextInt(types.size())),
          content
      );

      publisher.publish(message);

      System.out.println(String.format("Published message: %s of %s", m, numMessages));
    }
  }

  private static Message makeMessage(String channel, String topic, String type, String content) {
    MessageHeader header = new MessageHeader();
    header.setParentId(null);

    //header.setPublisherId(halogenPublisherId);  // Set by halPublisher
    header.setChannel(channel);
    header.setTopic(topic);
    header.setType(type);

    Message<String> message = new BasicMessage<>();
    message.setHeader(header);
    message.setContent(content);

    return message;
  }
}
