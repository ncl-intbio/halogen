/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.examples;

import com.google.gson.Gson;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.HalSubscriberWsClient;
import com.microbasecloud.util.GsonFactory;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class ExampleSubscriber {

  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Usage:");
      System.out.println("<halogen_url> <int: subscriber_id>");
      System.exit(1);
    }

    String halogenUrl = args[0];
    long subscriberId = Long.parseLong(args[1]);


    HalAuth subscriberCreds = new HalAuth(new UUID(0L, subscriberId), new UUID(0L, 0L));

    Gson gson = GsonFactory.make();

    HalSubscriber subscriber = new HalSubscriberWsClient(gson, halogenUrl, subscriberCreds);

    int count = 0;
    while (true) {
      List<Message<String>> msgs = subscriber.consume(100, true);
      if (msgs.isEmpty()) {
        ThreadUtils.sleep(1000L);
        continue;
      }

      for (Message<String> msg : msgs) {
        System.out.println(String.format("Received count %s. Message: %s with channel: %s, topic: %s, type: %s Content: %s",
            count,
            msg.getHeader().getUid(),
            msg.getHeader().getChannel(),
            msg.getHeader().getTopic(),
            msg.getHeader().getType(),
            msg.getContent()
        ));
        count++;
      }
    }
  }

}
