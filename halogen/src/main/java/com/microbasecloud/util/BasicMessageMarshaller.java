/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.util;

import com.google.gson.*;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.MessageHeader;
import com.microbasecloud.halogen.wrappers.BasicMessage;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * A full marshaller for <code>EventMessage</code> instances (in contrast to the partial serializer,
 * <code>EventMessagePayloadOnlyMarshaller</code>.
 * @author Keith Flanagan
 */
public class BasicMessageMarshaller implements JsonSerializer<Message>, JsonDeserializer<Message> {
  private static final Logger logger = Logger.getLogger(BasicMessageMarshaller.class.getName());

  private Map<String, Class> contentTypeCache = new HashMap<>();

  public BasicMessageMarshaller() {
  }

  @Override
  public JsonElement serialize(Message src, Type typeOfSrc, JsonSerializationContext context) {
    try {
      JsonObject ser = new JsonObject();
      ser.add("header", context.serialize(src.getHeader()));
      ser.add(".contentClassName", context.serialize(src.getContent().getClass().getName()));
      ser.add("content", context.serialize(src.getContent()));

      return ser;
    } catch (Exception e) {
      throw new JsonParseException("Failed to serialize object: "+src.toString(), e);
    }
  }

  @Override
  public Message deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    try {
      if (!json.isJsonObject()) {
        throw new JsonParseException("Expected a JsonObject. Got: "+json.getAsString());
      }
      JsonObject obj = json.getAsJsonObject();

      try {
        BasicMessage event = new BasicMessage();
        event.setHeader(context.deserialize(obj.get("header"), MessageHeader.class));
        String contentClassName = obj.get(".contentClassName").getAsString();
        Class contentClass = contentTypeCache.get(contentClassName);
        if (contentClass == null) {
          contentClass = Class.forName(contentClassName);
          contentTypeCache.put(contentClassName, contentClass);
        }
        event.setContent(context.deserialize(obj.get("content"), contentClass));
        return event;
      } catch (Exception e) {
        logger.info("Error while parsing. Possibly malformed JSON?");
        throw new JsonParseException("Error while parsing. Possibly malformed JSON?", e);
      }
    } catch (Exception e) {
//      e.printStackTrace();
//      logger.info("Failed to parse text: "+json.getAsString());
      throw new JsonParseException("Failed to parse!", e);
    }
  }
}
