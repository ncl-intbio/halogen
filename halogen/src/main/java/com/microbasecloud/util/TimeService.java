package com.microbasecloud.util;

/**
 * Time service abstraction interface.
 *
 * @author jimi
 */
public interface TimeService {

  /**
   * Get the current time in milliseconds.
   *
   * @return the current time in milliseconds.
   */
  long getNow();
}
