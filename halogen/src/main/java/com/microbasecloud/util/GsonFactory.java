/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.wrappers.BasicMessage;
import com.microbasecloud.halogen.wrappers.ContentWrapper;

import java.util.Date;

/**
 * @author Keith Flanagan
 */
public class GsonFactory {
  public static Gson make() {
    return new GsonBuilder()
        .enableComplexMapKeySerialization() // If not enabled, gson will serialize map keys with toString() [!]
        //.setDateFormat(DateFormat.FULL)
        .setPrettyPrinting()

        //.setDateFormat("yyyy-mm-dd hh:mm:ss.ms")
        //.setDateFormat(DateFormat.FULL, DateFormat.FULL)

        //.registerTypeAdapter(Date.class, new DateMarshaller())
//        .registerTypeAdapter(Message.class, new BasicMessageMarshaller())
//        .registerTypeAdapter(BasicMessage.class, new BasicMessageMarshaller())
        .registerTypeAdapter(Date.class, new DateAsTimestampMarshaller())
        .registerTypeAdapter(ContentWrapper.class, new ContentWrapperMarshaller())
//        .registerTypeAdapter(ContentWrapper.class, new ContentWrapperMarshaller2())

        .registerTypeAdapter(Message.class, new BasicMessageMarshaller())      // Needed for deserialization
        .registerTypeAdapter(BasicMessage.class, new BasicMessageMarshaller()) // Needed for serialization

        .create();
  }
}
