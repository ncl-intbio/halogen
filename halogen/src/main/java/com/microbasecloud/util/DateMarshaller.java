/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.util;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Keith Flanagan
 */
public class DateMarshaller implements JsonSerializer<Date>, JsonDeserializer<Date> {
  private final SimpleDateFormat formatter;

  public DateMarshaller() {
    formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  @Override
  public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
    try {
      JsonObject obj = new JsonObject();
      obj.addProperty("dateStr", formatter.format(src));
      obj.addProperty("dateMs", src.getTime());

      return obj;
    } catch (Exception e) {
      throw new JsonParseException("Failed to serialize object: "+src.toString(), e);
    }
  }

  @Override
  public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
    try {
      if (!json.isJsonObject()) {
        throw new JsonParseException("Expected a JsonObject");
      }
      JsonObject obj = json.getAsJsonObject();
      return formatter.parse(obj.get("dateStr").getAsString());
    } catch (Exception e) {
      throw new JsonParseException("Failed to parse!", e);
    }
  }
}
