package com.microbasecloud.util;

/**
 * Basic implementation of time service using standard Java API calls
 * based on local system time.
 *
 * @author jimi
 */
public class SystemTimeService implements TimeService {
  /**
   * Returns the current system time in milliseconds.
   *
   * Implemented via <code>System.currentTimeMillis()</code>.
   *
   * @return the current system time in milliseconds.
   */
  @Override
  public long getNow() {
    return System.currentTimeMillis();
  }

  private static final SystemTimeService staticInstance = new SystemTimeService();

  /**
   * A static instance of the class.
   *
   * @return a static instance of the class.
   */
  public static SystemTimeService getStaticInstance()
  {
    return staticInstance;
  }
}
