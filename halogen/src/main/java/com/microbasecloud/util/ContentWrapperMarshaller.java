/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.util;

import com.google.gson.*;
import com.microbasecloud.halogen.wrappers.ContentWrapper;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class ContentWrapperMarshaller implements JsonSerializer<ContentWrapper>, JsonDeserializer<ContentWrapper> {
  private static final Logger logger = Logger.getLogger(ContentWrapperMarshaller.class.getName());

  private Map<String, Class> contentTypeCache = new HashMap<>();

  public ContentWrapperMarshaller() {
  }

  @Override
  public JsonElement serialize(ContentWrapper src, Type typeOfSrc, JsonSerializationContext context) {
    try {
      JsonObject ser = new JsonObject();
      ser.add(".class", context.serialize(src.getContent().getClass().getName()));
      ser.add("content", context.serialize(src.getContent()));

      return ser;
    } catch (Exception e) {
      throw new JsonParseException("Failed to serialize object: "+src.toString(), e);
    }
  }

  @Override
  public ContentWrapper deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    try {
      if (!json.isJsonObject()) {
        throw new JsonParseException("Expected a JsonObject. Got: "+json.getAsString());
      }
      JsonObject obj = json.getAsJsonObject();

      try {
        ContentWrapper event = new ContentWrapper();
//        event.setId(context.deserialize(obj.get("id"), UUID.class));
//        event.setInResponseTo(context.deserialize(obj.get("inResponseTo"), UUID.class));
//        event.setDate(context.deserialize(obj.get("date"), Date.class));
        String contentClassName = obj.get(".class").getAsString();
        Class contentClass = contentTypeCache.get(contentClassName);
        if (contentClass == null) {
          contentClass = Class.forName(contentClassName);
          contentTypeCache.put(contentClassName, contentClass);
        }
        event.setContent(context.deserialize(obj.get("content"), contentClass));
        return event;
      } catch (Exception e) {
        throw new JsonParseException("Error while parsing. Possibly malformed JSON?", e);
      }
    } catch (Exception e) {
      throw new JsonParseException("Failed to parse!", e);
    }
  }
}
