/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.util;

import com.google.gson.Gson;
import com.microbasecloud.chlorine.State;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.publisher.HalPublisherWsClient;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.HalSubscriberWsClient;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.logging.Logger;

/**
 * A utility that can be used to block a thread until it has been confirmed that a specified publisher or
 * subscriber has been registered. This can be useful in situations where a program has must be started up before
 * its subscriber or publisher entries can be published. In these cases, we don't want the program to proceed
 * until the relevant entities have been created.
 *
 * @author Keith Flanagan
 */
public class BlockUntilRegistered {
  private static final Logger logger = Logger.getLogger(BlockUntilRegistered.class.getName());

  public static void waitUntilPublisherRegistered(String publisherWsUrl, HalAuth publisherDetails) {
    Gson gson = GsonFactory.make();
    HalPublisherWsClient publisher = new HalPublisherWsClient(gson, publisherWsUrl, publisherDetails);
    waitUntilPublisherRegistered(publisher);
  }


  public static void waitUntilPublisherRegistered(HalPublisher publisher) {
    boolean registered;
    do {
      try {
        registered = publisher.isRegistered();
        if (!registered) {
          logger.info("Waiting for publisher to be registered ...");
          ThreadUtils.sleep(5000);
        }
      } catch (Exception e) {
        e.printStackTrace();
        logger.info("An error occurred while querying whether a publisher existed. Will retry.");
        ThreadUtils.sleep(15000L);
      }
    } while (!publisher.isRegistered());
  }

  public static void waitUntilSubscriberRegistered(String subscriberWsUrl, HalAuth subscriberDetails) {
    Gson gson = GsonFactory.make();
    HalSubscriberWsClient subscriber = new HalSubscriberWsClient(gson, subscriberWsUrl, subscriberDetails);
    waitUntilSubscriberRegistered(subscriber);
  }


  public static void waitUntilSubscriberRegistered(HalSubscriber subscriber) {
    boolean registered;
    do {
      try {
        registered = subscriber.isRegistered();
        if (!registered) {
          logger.info("Waiting for subscriber to be registered ...");
          ThreadUtils.sleep(5000);
        }
      } catch (Exception e) {
        e.printStackTrace();
        logger.info("An error occurred while querying whether a subscriber existed. Will retry.");
        ThreadUtils.sleep(15000L);
      }
    } while (!subscriber.isRegistered());
  }
}
