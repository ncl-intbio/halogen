/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.wrappers.ContentWrapper;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Keith Flanagan
 */
public class GsonFactoryScratch {


  public static void main(String[] args) {
//    test1(); // Works
//    test2(); // Will fail - we don't support generic types
    test3(); // Works (same as test2(), but contains a wrapper object)
  }

  public static void test1() {
    Gson gson = GsonFactory.make();

    //List<String> content = new ArrayList<>();
    List<String> content = new LinkedList<>();
    content.add("foo");
    content.add("bar");
    content.add("baz");

    ContentWrapper<List<String>> test1 = new ContentWrapper<>();
    test1.setContent(content);

    String test1Json = gson.toJson(test1);
    System.out.println(test1Json);


    ContentWrapper<List<String>> test1b = gson.fromJson(test1Json, new TypeToken<ContentWrapper<List<String>>>(){}.getType());
    System.out.println(test1b.getClass().getName());
    System.out.println(test1b.getContent().getClass().getName());
    System.out.println(test1b.getContent().get(0).getClass().getName());

    System.out.println("----");

    ContentWrapper test1c = gson.fromJson(test1Json, ContentWrapper.class);
    System.out.println(test1c.getClass().getName());
    System.out.println(test1c.getContent().getClass().getName());
    System.out.println(((List<String>) test1c.getContent()).get(0).getClass().getName());


    System.out.println("----");

    ContentWrapper test1d = gson.fromJson(test1Json, ContentWrapper.class);
    System.out.println(test1d.getClass().getName());
    System.out.println(test1d.getContent().getClass().getName());
    System.out.println(((List) test1d.getContent()).get(0).getClass().getName());



//    System.out.println(gson.toJson(new Date()));
//    MessageHeader foo = new MessageHeader();
//    foo.setDate(new Date());
//    System.out.println(gson.toJson(foo));


//    ContentWrapper bar = new ContentWrapper(new Date());
//    System.out.println(gson.toJson(bar));
//    ContentWrapper baz = gson.fromJson(gson.toJson(bar), ContentWrapper.class);
//    System.out.println(baz);
//    System.out.println(baz.getWrapped());
//    System.out.println(baz.getWrapped().getClass().getName());

//    Message<Date> foo = new BasicMessage<>();
//    foo.setHeader(new MessageHeader());
//    foo.setContent(new Date());
//
//    String json = gson.toJson(foo);
//    System.out.println(json);
//
//    Message<Date> bar = gson.fromJson(json, BasicMessage.class);
//    System.out.println(bar.getContent().toString());
//
//    System.out.println("----");
//    StringMessage stringMessage = EventMessage.toStringMessage(gson, foo);
//    System.out.println(stringMessage);
//    System.out.println("----");
//    EventMessage deserialized = EventMessage.fromStringMessage(gson, stringMessage);
//    System.out.println(deserialized.getContent().getClass().getName());
//
//    EventMessage<Date> deserialized2 = EventMessage.fromStringMessage(gson, stringMessage);
//    System.out.println(deserialized2.getContent().getClass().getName());
//    System.out.println(deserialized2.getContent().getTime());
//
//
//    System.out.println("+++++");
//
//    List<String> data = List.of("foo", "bar", "baz");
//
//    EventMessage<List<String>> foo2 = new EventMessage<>();
//    foo2.setHeader(new MessageHeader());
//    foo2.setContent(data);
//
//    System.out.println(gson.toJson(foo2));
//    System.out.println("+++++");
//    System.out.println(gson.fromJson(gson.toJson(foo2), EventMessage.class).getContent().getClass().getName());
//
//    EventMessage<List<String>> foo3 = gson.fromJson(gson.toJson(foo2), EventMessage.class);
//    System.out.println(foo3.getContent().get(2));


  }

  static class Test2 {
    String foo;

    public Test2() {
    }

    public Test2(String foo) {
      this.foo = foo;
    }

    public String getFoo() {
      return foo;
    }

    public void setFoo(String foo) {
      this.foo = foo;
    }
  }

  static class Test2Container {
    List<Test2> container = new LinkedList<>();

    public List<Test2> getContainer() {
      return container;
    }

    public void setContainer(List<Test2> container) {
      this.container = container;
    }
  }

  public static void test2() {
    Gson gson = GsonFactory.make();

    List<Test2> content = new LinkedList<>();
    content.add(new Test2("foo"));
    content.add(new Test2("bar"));
    content.add(new Test2("baz"));

    ContentWrapper<List<Test2>> test1 = new ContentWrapper<>();
    test1.setContent(content);

    String test1Json = gson.toJson(test1);
    System.out.println(test1Json);


    ContentWrapper<List<Test2>> test1b = gson.fromJson(test1Json, new TypeToken<ContentWrapper<List<Test2>>>(){}.getType());
    System.out.println(test1b.getClass().getName());
    System.out.println(test1b.getContent().getClass().getName());
    System.out.println(test1b.getContent().get(0).getClass().getName());

    System.out.println("----");

    ContentWrapper test1c = gson.fromJson(test1Json, ContentWrapper.class);
    System.out.println(test1c.getClass().getName());
    System.out.println(test1c.getContent().getClass().getName());
    System.out.println(((List<Test2>) test1c.getContent()).get(0).getClass().getName());


    System.out.println("----");

    ContentWrapper test1d = gson.fromJson(test1Json, ContentWrapper.class);
    System.out.println(test1d.getClass().getName());
    System.out.println(test1d.getContent().getClass().getName());
    System.out.println(((List) test1d.getContent()).get(0).getClass().getName());
  }

  public static void test3() {
    Gson gson = GsonFactory.make();

    Test2Container content = new Test2Container();
    content.container.add(new Test2("foo"));
    content.container.add(new Test2("bar"));
    content.container.add(new Test2("baz"));

    ContentWrapper<Test2Container> test1 = new ContentWrapper<>();
    test1.setContent(content);

    String test1Json = gson.toJson(test1);
    System.out.println(test1Json);


    ContentWrapper<Test2Container> test1b = gson.fromJson(test1Json, new TypeToken<ContentWrapper<Test2Container>>(){}.getType());
    System.out.println(test1b.getClass().getName());
    System.out.println(test1b.getContent().getClass().getName());
    System.out.println(test1b.getContent().container.get(0).getClass().getName());

    System.out.println("----");

    ContentWrapper test1c = gson.fromJson(test1Json, ContentWrapper.class);
    System.out.println(test1c.getClass().getName());
    System.out.println(test1c.getContent().getClass().getName());
    System.out.println(((Test2Container) test1c.getContent()).container.get(0).getClass().getName());
  }
}
