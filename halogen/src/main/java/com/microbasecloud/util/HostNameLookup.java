package com.microbasecloud.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 * Looking up hostnames in Java (and indeed any language) is often fraught with difficulty. There is apparently
 * no clean/infallible way to do this in a completely platform-specific environment.
 *
 * There are multiple ways in which the hostname of a system can be determined (environment variables, parsing the
 * network configuration, command line utilities, etc). Depending on the operating system,
 * (mis)configuration, or complexity of the environment, any one method may fail. Even Java's own
 * <code>getHostName</code> method isn't infallible.
 *
 * Therefore, sadly, it looks as though we need to resort to multiple methods for hostname lookups in the <i>hope</i>
 * that one of them actually works in a given situation(!)
 *
 *
 * See this thread for more information:
 * <ul>
 *   <li>http://stackoverflow.com/questions/7348711/recommended-way-to-get-hostname-in-java</li>
 * </ul>
 *
 * @author Keith Flanagan
 */
public class HostNameLookup {
  private static final Logger logger = Logger.getLogger(HostNameLookup.class.getSimpleName());

  public static String lookupHostname() {
    /*
     * Use Java's built-in mechanism to look up the hostname.
     * This won't work in every configuration though.
     */
    String hostname;
    try {
      //hostname = InetAddress.getLocalHost().getHostName();
      hostname = InetAddress.getLocalHost().getCanonicalHostName();
      if (hostname != null && hostname.length() > 0) {
        logger.info("Returning hostname: "+hostname+", as determined by Java's InetAddress.getLocalHost()");
        return hostname;
      }
    } catch (UnknownHostException e) {
      logger.info("Lookup by InetAddress.getLocalHost().getHostName() failed.");
    }

    /*
     * Read the environment variable present on Unix-like machines
     */
    hostname = System.getenv("HOSTNAME");
    if  (hostname != null && hostname.length() > 0) {
      logger.info("Returning hostname: "+hostname+", as determined by the environment variable HOSTNAME");
      return hostname;
    }

    /*
     * Read the environment variable present on Windows machines
     */
    hostname= System.getenv("COMPUTERNAME");
    if  (hostname != null && hostname.length() > 0) {
      logger.info("Returning hostname: "+hostname+", as determined by the environment variable COMPUTERNAME");
      return hostname;
    }

    /*
     * A last-ditch attempt to read the hostname via the 'hostname' external command
     */
//    try {
//        //FIXME: Implement this!
//      Runtime.getRuntime().exec("hostname");
//      if  (hostname != null && hostname.length() > 0) {
//        logger.info("Returning hostname: "+hostname+", as determined by the external 'hostname' command.");
//        return hostname;
//      }
//    } catch (Exception e) {
//      logger.info("Failed to lookup by the external 'hostname' command.");
//    }

    throw new RuntimeException("Failed to determine this machine's hostname!");
  }
}
