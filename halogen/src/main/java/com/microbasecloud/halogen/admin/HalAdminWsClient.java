/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.halogen.admin;

import com.google.gson.Gson;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.WsClientBase;
import com.microbasecloud.halogen.store.HalogenStorageException;
import com.microbasecloud.halogen.subscriber.Subscription;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HalAdminWsClient extends WsClientBase implements HalAdmin {

  private final HalAuth halAuth;
//  private final UUID adminId;
//  private final UUID adminSecret;


  public HalAdminWsClient(Gson gson, String wsUrl, HalAuth halAuth) {
    super(gson, wsUrl);
    this.halAuth = halAuth;
  }

  public HalAdminWsClient(Gson gson, String wsUrl, UUID adminId, UUID adminSecret) {
    this(gson, wsUrl, new HalAuth(adminId, adminSecret));
  }

  @Override
  public void makeAdminAccount(UUID newAdminId, UUID newAdminSecretKey) throws HalogenStorageException {
    WsMakeAdminAccountRequest adminReq = new WsMakeAdminAccountRequest();
    adminReq.setAdminId(halAuth.getId());
    adminReq.setAdminSecret(halAuth.getSecret());
    adminReq.setNewAdminId(newAdminId);
    adminReq.setNewAdminSecret(newAdminSecretKey);

    HttpPost req = new HttpPost(wsUrl + "/admin/makeAdminAccount");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(adminReq)));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public void makeSubscriber(Subscription subscription, UUID subscriberId, UUID secretKey) throws HalogenStorageException {
    WsMakeSubscriberRequest subReq = new WsMakeSubscriberRequest();
    subReq.setAdminId(halAuth.getId());
    subReq.setAdminSecret(halAuth.getSecret());
    subReq.setSubscriberId(subscriberId);
    subReq.setSubscriberSecret(secretKey);
    subReq.setSubscription(subscription);

    HttpPost req = new HttpPost(wsUrl + "/admin/makeSubscriber");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(subReq)));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public void makePublisher(UUID publisherId, UUID secretKey) throws HalogenStorageException {
    WsMakePublisherRequest pubReq = new WsMakePublisherRequest();
    pubReq.setAdminId(halAuth.getId());
    pubReq.setAdminSecret(halAuth.getSecret());
    pubReq.setPublisherId(publisherId);
    pubReq.setPublisherSecret(secretKey);

    HttpPost req = new HttpPost(wsUrl + "/admin/makePublisher");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(pubReq)));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }
}
