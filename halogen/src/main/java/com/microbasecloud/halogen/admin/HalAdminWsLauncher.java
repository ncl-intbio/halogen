/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.admin;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.fluorine.ObjectNotFoundException;
import com.microbasecloud.fluorine.ObjectStoreException;
import com.microbasecloud.halogen.SecurityMode;
import com.microbasecloud.halogen.publisher.PublisherData;
import com.microbasecloud.halogen.publisher.PublisherStore;
import com.microbasecloud.halogen.store.HalogenStorageException;
import com.microbasecloud.halogen.subscriber.SubscriberData;
import com.microbasecloud.halogen.subscriber.SubscriberStore;
import com.microbasecloud.util.GsonFactory;
import com.microbasecloud.util.SystemTimeService;
import com.microbasecloud.util.TimeService;
import com.scalesinformatics.dbpool.DistributedJdbcPool;
import com.scalesinformatics.dbpool.HikariSqlPool;
import org.apache.commons.cli.*;

import java.util.UUID;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;
import static spark.Spark.*;

/**
 * @author Keith Flanagan
 */
public class HalAdminWsLauncher {

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "HalPublisherWsLauncher.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("dbUrl").hasArgs().withDescription("Admin database URL (e.g.: jdbc:postgresql://hostname:port/dbname)").create());
    options.addOption(withLongOpt("dbUser").hasArgs().withDescription("Admin database username").create());
    options.addOption(withLongOpt("dbPass").hasArgs().withDescription("Admin database password").create());
    options.addOption(withLongOpt("dbPoolSize").hasArgs().withDescription("Admin database connection pool size").create());
    options.addOption(withLongOpt("secure").hasArgs().withDescription("Set 'true' if you require all operations to be authenticated. " +
        "Set 'false' to allow any user to connect and interact with this service. Default = true.").create());
    options.addOption(withLongOpt("bootstrap-admin").hasArgs().withDescription("[Optional] Allows specifying an initial admin 'user'. " +
        "You should specify two UUIDs: the initial admin ID, and the associated 'secret' key. " +
        "This user is not stored in the database.").create());

    if (args.length == 0) {
      printHelpExit(options);
    }


    String dbUrl;
    String dbUser;
    String dbPass;
    int dbPoolSize;
    final SecurityMode securityMode;

    final UUID bootstrapAdminId;
    final UUID bootstrapAdminSecret;

    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("dbUrl")) {
        dbUrl = line.getOptionValue("dbUrl");
      } else {
        throw new RuntimeException("You forgot to specify a database URL");
      }

      if (line.hasOption("dbUser")) {
        dbUser = line.getOptionValue("dbUser");
      } else {
        throw new RuntimeException("You forgot to specify a database username");
      }

      if (line.hasOption("dbPass")) {
        dbPass = line.getOptionValue("dbPass");
      } else {
        throw new RuntimeException("You forgot to specify a database password");
      }

      if (line.hasOption("dbPoolSize")) {
        dbPoolSize = Integer.parseInt(line.getOptionValue("dbPoolSize"));
      } else {
        dbPoolSize = 8;
      }

      if (line.hasOption("secure") && !Boolean.parseBoolean(line.getOptionValue("secure"))) {
        securityMode = SecurityMode.INSECURE;
      } else {
        securityMode = SecurityMode.SECURE;
      }

      if (line.hasOption("bootstrap-admin")) {
        String[] adminStrings = line.getOptionValues("bootstrap-admin");
        bootstrapAdminId = UUID.fromString(adminStrings[0]);
        bootstrapAdminSecret = UUID.fromString(adminStrings[1]);
      } else {
        bootstrapAdminId = null;
        bootstrapAdminSecret = null;
      }
    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    DistributedJdbcPool connFact = HikariSqlPool.make(dbUser, dbPass, dbUrl, dbPoolSize, dbPoolSize);

    port(5000);
    hostAdminService("/admin", connFact, securityMode, bootstrapAdminId,bootstrapAdminSecret);
  }

  public static void hostAdminService(String basePath, DistributedJdbcPool connFact, SecurityMode securityMode,
                                      UUID bootstrapAdminId, UUID bootstrapAdminSecret) {
    if (!basePath.isEmpty() && !basePath.startsWith("/")) {
      throw new RuntimeException("The base path should either be empty, or start with a '/'. Actual path was: "+basePath);
    }

    TimeService clock = new SystemTimeService();

    Gson gson = GsonFactory.make();

    AdminStore adminStore = new AdminStore(connFact, "admin", gson, new TypeToken<AdminData>() {}, clock);
    SubscriberStore subscriberStore = new SubscriberStore(connFact, "subscribers", gson, new TypeToken<SubscriberData>() {}, clock);
    PublisherStore publisherStore = new PublisherStore(connFact, "publishers", gson, new TypeToken<PublisherData>() {}, clock);
    //HalogenStorage<String> storageService = new HalogenStorageSql(connFact, "messages");


    exception(Exception.class, (e, request, response) -> {
      e.printStackTrace();
      response.status(500);
      response.body("HalogenStorageException: "+e.getMessage()+"\n");
    });

    post(basePath + "/makeAdminAccount", (req, res) -> {
      WsMakeAdminAccountRequest adminReq = gson.fromJson(req.body(), WsMakeAdminAccountRequest.class);

      if (!isAdminAllowed(adminStore, securityMode, bootstrapAdminId, bootstrapAdminSecret, adminReq.getAdminId(), adminReq.getAdminSecret())) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      if (subscriberStore.existsByUid(adminReq.getNewAdminId())) {
        throw new HalogenStorageException("The requested admin account already exists!");
      }

      AdminData adminAcc = new AdminData();
      adminAcc.setUid(adminReq.getNewAdminId());
      adminAcc.setSecret(adminReq.getNewAdminSecret());

      adminStore.put(adminAcc);

      res.status(202); // HTTP accepted
      return "Accepted";
    });

    post(basePath + "/makeSubscriber", (req, res) -> {
      WsMakeSubscriberRequest subReq = gson.fromJson(req.body(), WsMakeSubscriberRequest.class);

      if (!isAdminAllowed(adminStore, securityMode, bootstrapAdminId, bootstrapAdminSecret, subReq.getAdminId(), subReq.getAdminSecret())) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      if (subscriberStore.existsByUid(subReq.getSubscriberId())) {
        throw new HalogenStorageException("The requested subscriber already exists!");
      }

      SubscriberData subscriber = new SubscriberData();
      subscriber.setUid(subReq.getSubscriberId());
      subscriber.setSecretKey(subReq.getSubscriberSecret());
      subscriber.setEnabled(true);
      subscriber.setSubscription(subReq.getSubscription());

      subscriberStore.put(subscriber);

      res.status(202); // HTTP accepted
      return "Accepted";
    });

    post(basePath + "/makePublisher", (req, res) -> {
      WsMakePublisherRequest pubReq = gson.fromJson(req.body(), WsMakePublisherRequest.class);

      if (!isAdminAllowed(adminStore, securityMode, bootstrapAdminId, bootstrapAdminSecret, pubReq.getAdminId(), pubReq.getAdminSecret())) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      if (publisherStore.existsByUid(pubReq.getPublisherId())) {
        throw new HalogenStorageException("The requested publisher already exists!");
      }

      PublisherData publisher = new PublisherData();
      publisher.setUid(pubReq.getPublisherId());
      publisher.setSecretKey(pubReq.getPublisherSecret());
      publisher.setEnabled(true);

      publisherStore.put(publisher);

      res.status(202); // HTTP accepted
      return "Accepted";
    });
  }



  private static boolean isAdminAllowed(AdminStore store, SecurityMode policy,
                                        UUID bootstrapAdminId, UUID bootstrapAdminSecret,
                                        UUID adminId, UUID secret) throws ObjectStoreException, ObjectNotFoundException {
    if (policy == SecurityMode.INSECURE) {
      return true;
    }

    if (bootstrapAdminId != null && adminId.equals(bootstrapAdminId) && secret.equals(bootstrapAdminSecret)) {
      return true;
    }

    if (!store.existsByUid(adminId)) {
      return false;
    }

    AdminData adminData = store.getByUid(adminId);
    return secret.equals(adminData.getSecret());
  }
}
