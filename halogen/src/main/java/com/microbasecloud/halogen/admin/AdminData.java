/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.microbasecloud.halogen.admin;

import com.microbasecloud.fluorine.AbstractVersionableObject;
import com.microbasecloud.halogen.subscriber.Subscription;

import java.util.Date;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class AdminData extends AbstractVersionableObject {

  private UUID secret;

  public AdminData() {
  }

  public AdminData(UUID adminId, UUID secret) {
    setUid(adminId);
    this.secret = secret;
  }

  public UUID getSecret() {
    return secret;
  }

  public void setSecret(UUID secret) {
    this.secret = secret;
  }
}
