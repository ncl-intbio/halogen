/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.admin;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class WsMakeAdminAccountRequest {
  private UUID adminId;
  private UUID adminSecret;

  private UUID newAdminId;
  private UUID newAdminSecret;

  public UUID getAdminId() {
    return adminId;
  }

  public void setAdminId(UUID adminId) {
    this.adminId = adminId;
  }

  public UUID getAdminSecret() {
    return adminSecret;
  }

  public void setAdminSecret(UUID adminSecret) {
    this.adminSecret = adminSecret;
  }

  public UUID getNewAdminId() {
    return newAdminId;
  }

  public void setNewAdminId(UUID newAdminId) {
    this.newAdminId = newAdminId;
  }

  public UUID getNewAdminSecret() {
    return newAdminSecret;
  }

  public void setNewAdminSecret(UUID newAdminSecret) {
    this.newAdminSecret = newAdminSecret;
  }
}
