/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.halogen;

import com.microbasecloud.halogen.wrappers.BasicMessage;

import java.util.UUID;

/**
 * A convenience class that makes Message instances with a common channel and topic.
 *
 * @author Keith Flanagan
 */
public class MessageFactory {
  private UUID publisherUid;
  private String channel;
  private String topic;
  private String type;

  public MessageFactory(UUID publisherUid, String channel, String topic, String type) {
    this.publisherUid = publisherUid;
    this.channel = channel;
    this.topic = topic;
    this.type = type;
  }

  public MessageFactory(UUID publisherUid, String channel, String topic) {
    this(publisherUid, channel, topic, null);
  }

  public MessageFactory(UUID publisherUid, String channel) {
    this(publisherUid, channel, null, null);
  }

  public <T> Message<T> make(T content) {
    return make(null, content);
  }

  public <T> Message<T> make(UUID inResponseTo, T content) {
    MessageHeader header = new MessageHeader();
    header.setParentId(inResponseTo);

    //header.setPublisherId(halogenPublisherId);  // Set by halPublisher
    header.setChannel(channel);
    header.setTopic(topic);
    header.setType(type);

    Message<T> message = new BasicMessage<>();
    message.setHeader(header);
    message.setContent(content);

    return message;
  }
}
