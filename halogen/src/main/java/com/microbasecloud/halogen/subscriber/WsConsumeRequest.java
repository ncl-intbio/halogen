/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.subscriber;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class WsConsumeRequest {
  private UUID subscriberId;
  private UUID secretKey;
  private int maxMessages;
  private boolean advancePointer;

  /**
   * Allows a subscriber to update the message pointer before perfoming the message consume operation.
   * This enables, for instance, a subscriber to backtrack over previous messages. For example, to
   * re-run a failed process. Alternatively, if the subscriber is aware of 'future' message IDs, it can use
   * this option to skip ahead in the message history.
   */
  private UUID overrideMessagePointer;


  public WsConsumeRequest() {
  }

  public WsConsumeRequest(UUID subscriberId, UUID secretKey, int maxMessages, boolean advancePointer) {
    this.subscriberId = subscriberId;
    this.secretKey = secretKey;
    this.maxMessages = maxMessages;
    this.advancePointer = advancePointer;
  }

  @Override
  public String toString() {
    return "WsConsumeRequest{" +
        "subscriberId=" + subscriberId +
        ", secretKey=" + secretKey +
        ", maxMessages=" + maxMessages +
        ", advancePointer=" + advancePointer +
        '}';
  }

  public UUID getSubscriberId() {
    return subscriberId;
  }

  public void setSubscriberId(UUID subscriberId) {
    this.subscriberId = subscriberId;
  }

  public UUID getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(UUID secretKey) {
    this.secretKey = secretKey;
  }

  public int getMaxMessages() {
    return maxMessages;
  }

  public void setMaxMessages(int maxMessages) {
    this.maxMessages = maxMessages;
  }

  public boolean isAdvancePointer() {
    return advancePointer;
  }

  public void setAdvancePointer(boolean advancePointer) {
    this.advancePointer = advancePointer;
  }

  public UUID getOverrideMessagePointer() {
    return overrideMessagePointer;
  }

  public void setOverrideMessagePointer(UUID overrideMessagePointer) {
    this.overrideMessagePointer = overrideMessagePointer;
  }
}
