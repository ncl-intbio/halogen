/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.subscriber;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class WsStepBackRequest {
  private UUID subscriberId;
  private UUID secretKey;

  public WsStepBackRequest() {
  }

  public WsStepBackRequest(UUID subscriberId, UUID secretKey) {
    this.subscriberId = subscriberId;
    this.secretKey = secretKey;
  }

  @Override
  public String toString() {
    return "WsSetMsgPointerRequest{" +
        "subscriberId=" + subscriberId +
        ", secretKey=" + secretKey +
        '}';
  }

  public UUID getSubscriberId() {
    return subscriberId;
  }

  public void setSubscriberId(UUID subscriberId) {
    this.subscriberId = subscriberId;
  }

  public UUID getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(UUID secretKey) {
    this.secretKey = secretKey;
  }
}
