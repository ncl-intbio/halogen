/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.subscriber;

import com.microbasecloud.fluorine.ObjectStoreException;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.store.HalogenStorage;
import com.microbasecloud.halogen.store.HalogenStorageException;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class HalSubscriberImpl implements HalSubscriber<String> {
  private static final Logger logger = Logger.getLogger(HalSubscriberImpl.class.getName());

  private final HalogenStorage<String> messageDb;
  private final SubscriberStore subscriberStore;

  // A very basic locking mechanism, that assumes this is the only accessor of the database.
  private final Set<UUID> lockedSubscribers;

  private final SubscriberData subscriber;

  public HalSubscriberImpl(HalogenStorage<String> messageDb, SubscriberStore subscriberStore,
                           Set<UUID> lockedSubscribers, SubscriberData subscriber) {
    this.messageDb = messageDb;
    this.subscriberStore = subscriberStore;
    this.lockedSubscribers = lockedSubscribers;
    this.subscriber = subscriber;
  }

  private void lockSubscriber() {
    boolean acquiredLock = false;
    UUID subscriberId = subscriber.getUid();
    while (!acquiredLock) {
      synchronized (lockedSubscribers) {
        /*
         * If the locked items set DOESN'T already contain this subscriber's ID, then add it.
         * In this case, we have successfully acquired the lock.
         *
         * If this subscriber's ID **ALREADY** exists, then another subscriber thread has already locked it.
         * In this case, then this **CURRENT** thread has not acquired the lock, and therefore must keep trying.
         */
        if (!lockedSubscribers.contains(subscriberId)) {
          lockedSubscribers.add(subscriberId);
          acquiredLock = true;
        }
      }
      if (!acquiredLock) {
        ThreadUtils.sleep(50);
      }
    }
  }

  private void unlockSubscriber() {
    synchronized (lockedSubscribers) {
      lockedSubscribers.remove(subscriber.getUid());
    }
  }


  @Override
  public List<Message<String>> consume(int maxMsgs, boolean advancePointer) throws HalogenStorageException {
    lockSubscriber();
    try {
      return doConsume(maxMsgs, advancePointer);
    } catch (Exception e) {
      throw new HalogenStorageException("Operation failed", e);
    } finally {
      unlockSubscriber();
    }
  }

  @Override
  public List<Message<String>> consumeAfter(UUID messagePointer, int maxMsgs, boolean advancePointer) throws HalogenStorageException {
    lockSubscriber();
    try {
      subscriber.setMessagePointer(messagePointer);
      return doConsume(maxMsgs, advancePointer);
    } catch (Exception e) {
      throw new HalogenStorageException("Operation failed", e);
    } finally {
      unlockSubscriber();
    }
  }

  private interface FirstMessageQuery {
    Message<String> query();
  }

  private interface ResumeMessageQuery {
    List<Message<String>> query(UUID afterMsgId, int maxMsgs);
  }

  private List<Message<String>> doConsume(int maxMsgs, boolean advanceMessagePointer) throws HalogenStorageException {
    Subscription subscription = subscriber.getSubscription();

    final List<Message<String>> result;
    // Special case where we want to list all messages (i.e. channel, topic and type are all NULL)
    if (subscription.getChannel() == null && subscription.getTopic() == null && subscription.getType() == null) {
      result = consumeMessages(maxMsgs,
          () -> messageDb.first(),
          (afterMsgUid, maxMsgs1) -> messageDb.listAfter(afterMsgUid, maxMsgs1, true));
    } else if (subscription.getChannel() != null && subscription.getTopic() != null && subscription.getType() != null) {
      result = consumeMessages(maxMsgs,
          () -> messageDb.firstByChannelTopicType(subscription.getChannel(), subscription.getTopic(), subscription.getType()),
          (afterMsgUid, maxMsgs1) -> messageDb.listByChannelTopicType(subscription.getChannel(), subscription.getTopic(), subscription.getType(), afterMsgUid, maxMsgs1, true));
    } else if (subscription.getChannel() != null && subscription.getTopic() != null) {
      result = consumeMessages(maxMsgs, () -> messageDb.firstByChannelAndTopic(subscription.getChannel(), subscription.getTopic()),
          (afterMsgUid, maxMsgs1) -> messageDb.listByChannelAndTopic(subscription.getChannel(), subscription.getTopic(), afterMsgUid, maxMsgs1, true));
    } else if (subscription.getChannel() != null && subscription.getType() != null) {
      result = consumeMessages(maxMsgs, () -> messageDb.first(subscription.getChannel(), subscription.getType()),
          (afterMsgUid, maxMsgs1) -> messageDb.listByChannelAndType(subscription.getChannel(), subscription.getType(), afterMsgUid, maxMsgs1, true));
    } else if (subscription.getChannel() != null) {
      result = consumeMessages(maxMsgs, () -> messageDb.first(subscription.getChannel()),
          (afterMsgUid, maxMsgs1) -> messageDb.listByChannel(subscription.getChannel(), afterMsgUid, maxMsgs1, true));
    } else if (subscription.getType() != null) {
      result = consumeMessages(maxMsgs, () -> messageDb.firstByType(subscription.getType()),
          (afterMsgUid, maxMsgs1) -> messageDb.listByType(subscription.getType(), afterMsgUid, maxMsgs1, true));
    } else {
      throw new HalogenStorageException("Unsupported query type: "+subscription.toString());
    }

    // Update the subscriber database, and then return the messages to the caller
    if (advanceMessagePointer) {
      try {
        subscriber.incrementVersion();
        subscriberStore.put(subscriber);
      } catch (Exception e) {
        throw new HalogenStorageException("Internal storage error", e);
      }
    }

    return result;
  }

  private List<Message<String>> consumeMessages(int maxMsgs,
                                                FirstMessageQuery firstMessageQuery,
                                                ResumeMessageQuery resumeFromLastQuery)
      throws HalogenStorageException {
    UUID lastSeenMsgId = subscriber.getMessagePointer();

    Message<String> firstMessage = lastSeenMsgId == null ? firstMessageQuery.query() : null;
    List<Message<String>> results = new ArrayList<>(maxMsgs);

    if (lastSeenMsgId == null && firstMessage == null) {
      // There are no messages at all - return an empty list
      return results;
    } else if (firstMessage != null) {
      // This subscriber is new - this is the first block of messages
      results.add(firstMessage);

      lastSeenMsgId = firstMessage.getHeader().getUid();
      maxMsgs = maxMsgs - 1;
    }

    // Find the next block of messages to consume
    results.addAll(resumeFromLastQuery.query(lastSeenMsgId, maxMsgs));

    // Update the subscriber bean with the new 'last seen' message
    if (!results.isEmpty()) {
      lastSeenMsgId = results.get(results.size() - 1).getHeader().getUid();
      subscriber.setMessagePointer(lastSeenMsgId);
    }
    return results;
  }

  @Override
  public void reset() throws HalogenStorageException {
    setMessagePointer(null);  // Reset to beginning of time
  }

  @Override
  public UUID stepBack() throws HalogenStorageException {
    lockSubscriber();
    try {
      Subscription s = subscriber.getSubscription();
      final List<Message<String>> result;
      if (s.getChannel() == null && s.getTopic() == null && s.getType() == null) {
        result = messageDb.listAfter(subscriber.getMessagePointer(), 1, false);
      } else if (s.getChannel() != null && s.getTopic() != null && s.getType() != null) {
        result = messageDb.listByChannelTopicType(s.getChannel(), s.getTopic(), s.getType(), subscriber.getMessagePointer(), 1, false);
      } else if (s.getChannel() != null && s.getTopic() != null) {
        result = messageDb.listByChannelAndTopic(s.getChannel(), s.getTopic(), subscriber.getMessagePointer(), 1, false);
      } else if (s.getChannel() != null && s.getType() != null) {
        result = messageDb.listByChannelAndType(s.getChannel(), s.getType(), subscriber.getMessagePointer(), 1, false);
      } else if (s.getChannel() != null) {
        result = messageDb.listByChannel(s.getChannel(), subscriber.getMessagePointer(), 1, false);
      } else if (s.getType() != null) {
        result = messageDb.listByType(s.getType(), subscriber.getMessagePointer(), 1, false);
      } else {
        throw new HalogenStorageException("Unsupported query type: "+s.toString());
      }

      subscriber.setMessagePointer(result.isEmpty() ? null : result.get(0).getHeader().getUid());
      subscriber.incrementVersion();
      subscriberStore.put(subscriber);
      return subscriber.getMessagePointer();
    } catch (Exception e) {
      throw new HalogenStorageException("Operation failed", e);
    } finally {
      unlockSubscriber();
    }
  }

  @Override
  public void setMessagePointer(UUID messageId) throws HalogenStorageException {
    lockSubscriber();
    try {
      subscriber.setMessagePointer(messageId);
      subscriber.incrementVersion();
      subscriberStore.put(subscriber);
    } catch (Exception e) {
      throw new HalogenStorageException("Operation failed", e);
    } finally {
      unlockSubscriber();
    }
  }

  @Override
  public boolean isRegistered() throws HalogenStorageException {
    try {
      return subscriberStore.existsByUid(subscriber.getUid());
    } catch (ObjectStoreException e) {
      throw new HalogenStorageException("Failed to look up subscriber: "+subscriber.getUid());
    }
  }
}
