package com.microbasecloud.halogen.subscriber;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.WsClientBase;
import com.microbasecloud.halogen.store.HalogenStorageException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author James McLaughlin
 * @author Keith Flanagan
 */
public class HalSubscriberWsClient extends WsClientBase implements HalSubscriber<String> {

  private final HalAuth halAuth;
//  private final UUID subscriberId;
//  private final UUID secretKey;


  public HalSubscriberWsClient(Gson gson, String wsUrl, HalAuth halAuth) {
    super(gson, wsUrl);
    this.halAuth = halAuth;
  }

  public HalSubscriberWsClient(Gson gson, String wsUrl, UUID subscriberId, UUID secretKey) {
    this(gson, wsUrl, new HalAuth(subscriberId, secretKey));
  }


  @Override
  public List<Message<String>> consume(int maxMsgs, boolean advancePointer) throws HalogenStorageException {
    return consume(new WsConsumeRequest(halAuth.getId(), halAuth.getSecret(), maxMsgs, advancePointer));
  }

  @Override
  public List<Message<String>> consumeAfter(UUID messagePointer, int maxMsgs, boolean advancePointer) throws HalogenStorageException {
    WsConsumeRequest consumeRequest = new WsConsumeRequest(halAuth.getId(), halAuth.getSecret(), maxMsgs, advancePointer);
    consumeRequest.setOverrideMessagePointer(messagePointer);
    return consume(consumeRequest);
  }

  private List<Message<String>> consume(WsConsumeRequest consumeRequest) throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/subscriber/consume");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(consumeRequest)));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return objectFromResponse(res, new TypeToken<List<Message<String>>>() {});
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }


  @Override
  public void reset() throws HalogenStorageException {
    setMessagePointer(null);
  }

  @Override
  public UUID stepBack() throws HalogenStorageException {
    WsStepBackRequest stepBackRequest = new WsStepBackRequest(halAuth.getId(), halAuth.getSecret());

    HttpPost req = new HttpPost(wsUrl + "/subscriber/stepBack");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(stepBackRequest)));
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return objectFromResponse(res, new TypeToken<UUID>() {});
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public void setMessagePointer(UUID messageId) throws HalogenStorageException {
    WsSetMsgPointerRequest pointerReq = new WsSetMsgPointerRequest(halAuth.getId(), halAuth.getSecret(), messageId);


    HttpPost req = new HttpPost(wsUrl + "/subscriber/setMessagePointer");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(pointerReq)));
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public boolean isRegistered() throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/subscriber/isRegistered/"+halAuth.getId());
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return objectFromResponse(res, new TypeToken<Boolean>() {});
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

}
