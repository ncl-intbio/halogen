/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.subscriber;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.store.HalogenStorageException;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public interface HalSubscriber<T> {
  default List<Message<T>> consume(int maxMsgs) {
    return consume(maxMsgs, true);
  }

  /**
   * Consume allows you to iterate through a set of messages of interest. Unless <code>reset()</code> is called,
   * a given subscriber will only see each matching message once. Messages are returned in publication order, as
   * stored by the underlying database.
   *
   * If multiple client machines call <code>consume</code> with the same <code>subscription</code> query, then
   * they will receive different batches of messages. This allows you to very easily implement a distributed
   * compute cluster for a particular 'queue' of messages, as defined by <code>subscription</code>.
   *
   * @param maxMsgs the maximum number of messages to return per call (up to some server-configured limit).
   * @return a list of messages that match <code>subscription</code>, in publication order.
   */
  List<Message<T>> consume(int maxMsgs, boolean advancePointer) throws HalogenStorageException;
  List<Message<T>> consumeAfter(UUID messagePointer, int maxMsgs, boolean advancePointer) throws HalogenStorageException;

//  Iterable<T> consumingIterator(UUID subscriberId, Subscription subscription, int batchSize) throws HalogenStorageException;
//  Stream<T> consumingStream(UUID subscriberId, Subscription subscription, int batchSize, long streamLimit) throws HalogenStorageException;

  /**
   * Resets the message pointer to the beginning of this subscription.
   * @throws HalogenStorageException
   */
  void reset() throws HalogenStorageException;

  /**
   * Moves the (remotely stored) message pointer back by one message.
   * If the message pointer is already at the beginning, then this call has no effect.
   * @return the UUID of the new message pointer location (or null, if at the start of the subscription)
   * @throws HalogenStorageException
   */
  UUID stepBack() throws HalogenStorageException;

  /**
   * Sets the subscriber message pointer to the specified message. This means that the next time
   * <code>consume</code> is called, the message(s) returned will be <i>after</i> <code>messageId</code>.
   * @param messageId
   * @throws HalogenStorageException
   */
  void setMessagePointer(UUID messageId) throws HalogenStorageException;

  boolean isRegistered() throws HalogenStorageException;
}
