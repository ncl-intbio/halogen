/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.subscriber;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.fluorine.ObjectNotFoundException;
import com.microbasecloud.fluorine.ObjectStoreException;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.SecurityMode;
import com.microbasecloud.halogen.store.HalogenStorage;
import com.microbasecloud.halogen.store.HalogenStorageSql;
import com.microbasecloud.util.GsonFactory;
import com.microbasecloud.util.SystemTimeService;
import com.microbasecloud.util.TimeService;
import com.scalesinformatics.dbpool.DistributedJdbcPool;
import com.scalesinformatics.dbpool.HikariSqlPool;
import org.apache.commons.cli.*;

import java.util.*;
import java.util.logging.Logger;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;
import static spark.Spark.*;

/**
 * @author Keith Flanagan
 */
public class HalSubscriberWsLauncher {
  private static final Logger logger = Logger.getLogger(HalSubscriberWsLauncher.class.getName());

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "HalSubscriberWsLauncher.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("dbUrl").hasArgs().withDescription("Message database URL (e.g.: jdbc:postgresql://hostname:port/dbname)").create());
    options.addOption(withLongOpt("dbUser").hasArgs().withDescription("Message database username").create());
    options.addOption(withLongOpt("dbPass").hasArgs().withDescription("Message database password").create());
    options.addOption(withLongOpt("dbPoolSize").hasArgs().withDescription("Message database connection pool size").create());
    options.addOption(withLongOpt("secure").hasArgs().withDescription("Set 'true' if you require all publishers and subscribers to be registered before accessing the system. " +
        "Set 'false' to allow any subscriber/publisher to connect. Default = true.").create());


    if (args.length == 0) {
      printHelpExit(options);
    }


    String dbUrl;
    String dbUser;
    String dbPass;
    int dbPoolSize;
    SecurityMode securityMode;


    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("dbUrl")) {
        dbUrl = line.getOptionValue("dbUrl");
      } else {
        throw new RuntimeException("You forgot to specify a database URL");
      }

      if (line.hasOption("dbUser")) {
        dbUser = line.getOptionValue("dbUser");
      } else {
        throw new RuntimeException("You forgot to specify a database username");
      }

      if (line.hasOption("dbPass")) {
        dbPass = line.getOptionValue("dbPass");
      } else {
        throw new RuntimeException("You forgot to specify a database password");
      }

      if (line.hasOption("dbPoolSize")) {
        dbPoolSize = Integer.parseInt(line.getOptionValue("dbPoolSize"));
      } else {
        dbPoolSize = 8;
      }

      if (line.hasOption("secure") && !Boolean.parseBoolean(line.getOptionValue("secure"))) {
        securityMode = SecurityMode.INSECURE;
      } else {
        securityMode = SecurityMode.SECURE;
      }
    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    DistributedJdbcPool connFact = HikariSqlPool.make(dbUser, dbPass, dbUrl, dbPoolSize, dbPoolSize);

    port(5000);
    hostSubscriberService("/subscriber", connFact, securityMode);
  }

  public static void hostSubscriberService(String basePath, DistributedJdbcPool connFact, SecurityMode securityMode) {
    if (!basePath.isEmpty() && !basePath.startsWith("/")) {
      throw new RuntimeException("The base path should either be empty, or start with a '/'. Actual path was: "+basePath);
    }

    TimeService clock = new SystemTimeService();

    Gson gson = GsonFactory.make();

    HalogenStorage<String> storageService = new HalogenStorageSql(connFact, "messages");
    SubscriberStore subscriberStore = new SubscriberStore(connFact, "subscribers", gson,
        new TypeToken<SubscriberData>() {}, clock);
    //HalSubscriber<StringMessage> subscriberService = new HalSubscriberImpl(storageService, subscriberStore);
    Set<UUID> subscriberLocks = new HashSet<>();



    exception(IllegalArgumentException.class, (e, request, response) -> {
      e.printStackTrace();
      response.status(400);
      response.body("IllegalArgumentException: "+e.getMessage()+"\n");
    });

    exception(ObjectNotFoundException.class, (e, request, response) -> {
      e.printStackTrace();
      response.status(400);
      response.body("ObjectNotFoundException: "+e.getMessage()+"\n");
    });


    exception(Exception.class, (e, request, response) -> {
      e.printStackTrace();
      response.status(500);
      response.body("IllegalArgumentException: "+e.getMessage()+"\n");
    });

    post(basePath + "/consume", (req, res) -> {
      WsConsumeRequest consumeRequest = gson.fromJson(req.body(), WsConsumeRequest.class);

      // Should throw ObjectNotFoundException if the subscriber doesn't exist.
      SubscriberData subscriber = subscriberStore.getByUid(consumeRequest.getSubscriberId());

      if (!isSubscriberAllowed(subscriber, securityMode,
          consumeRequest.getSubscriberId(), consumeRequest.getSecretKey())) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      HalSubscriber<String> subscriberService = new HalSubscriberImpl(
          storageService, subscriberStore, subscriberLocks, subscriber);

      List<Message<String>> messages;
      if (consumeRequest.getOverrideMessagePointer() == null) {
        messages = subscriberService.consume(consumeRequest.getMaxMessages(), consumeRequest.isAdvancePointer());
      } else {
        messages = subscriberService.consumeAfter(consumeRequest.getOverrideMessagePointer(),
            consumeRequest.getMaxMessages(), consumeRequest.isAdvancePointer());
      }

      String result = gson.toJson(messages);
      res.status(200);
      //logger.info("Result: "+result);
      return result;
    });

    post(basePath + "/stepBack", (req, res) -> {
      WsStepBackRequest stepBackRequest = gson.fromJson(req.body(), WsStepBackRequest.class);

      // Should throw ObjectNotFoundException if the subscriber doens't exist.
      SubscriberData subscriber = subscriberStore.getByUid(stepBackRequest.getSubscriberId());

      if (!isSubscriberAllowed(subscriber, securityMode,
          stepBackRequest.getSubscriberId(), stepBackRequest.getSecretKey())) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      HalSubscriber<String> subscriberService = new HalSubscriberImpl(
          storageService, subscriberStore, subscriberLocks, subscriber);

      UUID newPointerLocation = subscriberService.stepBack();

      res.status(202); // HTTP accepted
      return newPointerLocation == null ? null : gson.toJson(newPointerLocation);
    });


    post(basePath + "/setMessagePointer", (req, res) -> {
      WsSetMsgPointerRequest pointerRequest = gson.fromJson(req.body(), WsSetMsgPointerRequest.class);

      // Should throw ObjectNotFoundException if the subscriber doens't exist.
      SubscriberData subscriber = subscriberStore.getByUid(pointerRequest.getSubscriberId());

      if (!isSubscriberAllowed(subscriber, securityMode,
          pointerRequest.getSubscriberId(), pointerRequest.getSecretKey())) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      HalSubscriber<String> subscriberService = new HalSubscriberImpl(
          storageService, subscriberStore, subscriberLocks, subscriber);

      subscriberService.setMessagePointer(pointerRequest.getLastSeenMessage());

      res.status(202); // HTTP accepted
      return String.format("Subscriber %s is now set to message: %s", subscriber.getUid(), subscriber.getMessagePointer());
    });

    get(basePath + "/isRegistered/:subscriber_id", (req, res) -> {
      UUID subscriberId = UUID.fromString(req.params().get(":subscriber_id"));
      res.status(200);
      return gson.toJson(subscriberStore.existsByUid(subscriberId));
    });

  }

  private static int parseInt(String intString, int minIncl, int maxIncl) throws IllegalArgumentException {
    int val;
    try {
      val = Integer.parseInt(intString);
    } catch (Exception e) {
      throw new IllegalArgumentException("Input was not a valid integer: "+intString, e);
    }

    if (val < minIncl || val > maxIncl) {
      throw new IllegalArgumentException("Value "+intString+" was outside valid range: "+minIncl+" -- "+maxIncl);
    }
    return val;
  }



  private static boolean isSubscriberAllowed(SubscriberData subscriber, SecurityMode policy,
                                             UUID subId, UUID secretKey) throws ObjectNotFoundException, ObjectStoreException {
    if (policy == SecurityMode.INSECURE) {
      return true;
    }

//    if (!subscriberStore.existsByUid(subId)) {
//      logger.info(String.format("Subscriber not found: %s", subId));
//      return false;
//    }

    boolean secretKeyIsOk = secretKey.equals(subscriber.getSecretKey());
    if (!secretKeyIsOk) {
      logger.info(String.format("Subscriber %s had incorrect secret key: %s", subId, secretKey));
    }
    return secretKeyIsOk;
  }

}
