/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.microbasecloud.halogen.subscriber;

/**
 * @author Keith Flanagan
 */
public class Subscription {
  private String channel;
  private String topic;
  private String type;


  public Subscription() {
  }

  public Subscription(String channel, String topic, String type) {
    this.channel = channel;
    this.topic = topic;
    this.type = type;
  }

  @Override
  public String toString() {
    return "Subscription{" +
        ", channel='" + channel + '\'' +
        ", topic='" + topic + '\'' +
        ", type='" + type + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Subscription that = (Subscription) o;

    if (!channel.equals(that.channel)) return false;
    if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
    return type != null ? type.equals(that.type) : that.type == null;
  }

  @Override
  public int hashCode() {
    int result = channel.hashCode();
    result = 31 * result + (topic != null ? topic.hashCode() : 0);
    result = 31 * result + (type != null ? type.hashCode() : 0);
    return result;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

//  public Type getSubscriptionType() {
//    return subscriptionType;
//  }
//
//  public void setSubscriptionType(Type subscriptionType) {
//    this.subscriptionType = subscriptionType;
//  }

}
