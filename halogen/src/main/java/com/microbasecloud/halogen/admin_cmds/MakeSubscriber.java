/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.admin_cmds;

import com.microbasecloud.halogen.admin.HalAdmin;
import com.microbasecloud.halogen.admin.HalAdminWsClient;
import com.microbasecloud.halogen.subscriber.Subscription;
import com.microbasecloud.util.GsonFactory;
import org.apache.commons.cli.*;

import java.util.UUID;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;

/**
 * @author Keith Flanagan
 */
public class MakeSubscriber {

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "MakeSubscriber.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) throws Exception {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("url").hasArgs().withDescription("URL to the halogen admin service (e.g.: http://hostname:port").create());
    options.addOption(withLongOpt("adminId").hasArgs().withDescription("The admin user that will be used to create the new entry").create());
    options.addOption(withLongOpt("adminSecret").hasArgs().withDescription("The admin user password").create());

    options.addOption(withLongOpt("newAccountId").hasArgs().withDescription("The ID for the new account").create());
    options.addOption(withLongOpt("newAccountSecret").hasArgs().withDescription("The password for the new account").create());

    options.addOption(withLongOpt("channel").hasArgs().withDescription("The channel to subscribe to").create());
    options.addOption(withLongOpt("topic").hasArgs().withDescription("The topic to subscribe to").create());
    options.addOption(withLongOpt("type").hasArgs().withDescription("The message type to subscribe to").create());

    if (args.length == 0) {
      printHelpExit(options);
    }

    String wsUrl;
    UUID adminUser;
    UUID adminSecret;

    UUID newAccountId;
    UUID newAccountSecret;

    String channel = null;
    String topic = null;
    String type = null;

    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("url")) {
        wsUrl = line.getOptionValue("url");
      } else {
        throw new RuntimeException("You forgot to specify a service URL");
      }

      if (line.hasOption("adminId")) {
        adminUser = UUID.fromString(line.getOptionValue("adminId"));
      } else {
        throw new RuntimeException("You forgot to specify an admin account name");
      }

      if (line.hasOption("adminSecret")) {
        adminSecret = UUID.fromString(line.getOptionValue("adminSecret"));
      } else {
        throw new RuntimeException("You forgot to specify an admin account password");
      }

      if (line.hasOption("newAccountId")) {
        newAccountId = UUID.fromString(line.getOptionValue("newAccountId"));
      } else {
        System.out.println("You didn't specify an account ID. Generating a random one.");
        newAccountId = UUID.randomUUID();
      }

      if (line.hasOption("newAccountSecret")) {
        newAccountSecret = UUID.fromString(line.getOptionValue("newAccountSecret"));
      } else {
        System.out.println("You didn't specify an account secret key. Generating a random one.");
        newAccountSecret = UUID.randomUUID();
      }

      if (line.hasOption("channel")) {
        channel = line.getOptionValue("channel");
      }

      if (line.hasOption("topic")) {
        topic = line.getOptionValue("topic");
      }

      if (line.hasOption("type")) {
        type = line.getOptionValue("type");
      }

    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    System.out.println(String.format("Making a new subscriber: %s / %s", newAccountId, newAccountSecret));
    HalAdmin adminClient = new HalAdminWsClient(GsonFactory.make(), wsUrl, adminUser, adminSecret);

    Subscription subscription = new Subscription();
    subscription.setChannel(channel);
    subscription.setTopic(topic);
    subscription.setType(type);

    adminClient.makeSubscriber(subscription, newAccountId, newAccountSecret);
    System.out.println(String.format("Successfully created a new subscriber: %s / %s", newAccountId, newAccountSecret));
  }
}
