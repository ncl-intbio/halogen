/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.halogen.publisher;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.WsClientBase;
import com.microbasecloud.halogen.store.HalogenStorageException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class HalPublisherWsClient extends WsClientBase implements HalPublisher<String> {
  private static final Logger logger = Logger.getLogger(HalPublisherWsClient.class.getName());

  private final HalAuth halAuth;
//  private final UUID publisherId;
//  private final UUID secretKey;


  public HalPublisherWsClient(Gson gson, String wsUrl, HalAuth halAuth) {
    super(gson, wsUrl);
    this.halAuth = halAuth;
  }

  public HalPublisherWsClient(Gson gson, String wsUrl, UUID publisherId, UUID secretKey) {
    this(gson, wsUrl, new HalAuth(publisherId, secretKey));
  }

  @Override
  public void publish(Message<String> message) throws HalogenStorageException {
    publishAll(List.of(message));
  }

  @Override
  public void publishAll(List<Message<String>> messages) throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/publisher/publish");

    WsPublicationBean publication = new WsPublicationBean(halAuth.getId(), halAuth.getSecret(), messages);

    try {
      req.setHeader("Content-Type", "application/json");
      String json = gson.toJson(publication);
      //logger.info("Going to publish the following: "+json);
      req.setEntity(new StringEntity(json));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public boolean isRegistered() throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/publisher/isRegistered/"+halAuth.getId());
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return objectFromResponse(res, new TypeToken<Boolean>() {});
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listAllPublisherResponsesTo(UUID responsesToMsg, int maxMsgs, boolean ascending) throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/publisher/listAllPublisherResponsesTo");

    WsListAllPublisherResponsesToRequest requestBean = new WsListAllPublisherResponsesToRequest(
        halAuth.getId(), halAuth.getSecret(), responsesToMsg, maxMsgs, ascending);

    try {
      req.setHeader("Content-Type", "application/json");
      String json = gson.toJson(requestBean);
      req.setEntity(new StringEntity(json));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastMessagePublisherSent() throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/publisher/lastMessagePublisherSent");

    WsLastMessageSentRequest requestBean = new WsLastMessageSentRequest(halAuth.getId(), halAuth.getSecret());

    try {
      req.setHeader("Content-Type", "application/json");
      String json = gson.toJson(requestBean);
      req.setEntity(new StringEntity(json));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastMessagePublisherRespondedTo() throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/publisher/lastMessagePublisherRespondedTo");

    WsLastMessageRespondedToRequest requestBean = new WsLastMessageRespondedToRequest(halAuth.getId(), halAuth.getSecret());

    try {
      req.setHeader("Content-Type", "application/json");
      String json = gson.toJson(requestBean);
      req.setEntity(new StringEntity(json));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  private Message<String> messageFromResponse(HttpResponse res) {
    return objectFromResponse(res, new TypeToken<Message<String>>() {});
  }

  private List<Message<String>> messagesFromResponse(HttpResponse res) {
    return objectFromResponse(res, new TypeToken<List<Message<String>>>() {});
  }
}
