/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.publisher;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.fluorine.ObjectNotFoundException;
import com.microbasecloud.fluorine.ObjectStoreException;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.SecurityMode;
import com.microbasecloud.halogen.store.HalogenStorage;
import com.microbasecloud.halogen.store.HalogenStorageSql;
import com.microbasecloud.util.GsonFactory;
import com.microbasecloud.util.SystemTimeService;
import com.microbasecloud.util.TimeService;
import com.scalesinformatics.dbpool.DistributedJdbcPool;
import com.scalesinformatics.dbpool.HikariSqlPool;
import org.apache.commons.cli.*;

import java.util.Date;
import java.util.UUID;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;
import static spark.Spark.*;

/**
 * @author Keith Flanagan
 */
public class HalPublisherWsLauncher {

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "HalPublisherWsLauncher.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("dbUrl").hasArgs().withDescription("Message database URL (e.g.: jdbc:postgresql://hostname:port/dbname)").create());
    options.addOption(withLongOpt("dbUser").hasArgs().withDescription("Message database username").create());
    options.addOption(withLongOpt("dbPass").hasArgs().withDescription("Message database password").create());
    options.addOption(withLongOpt("dbPoolSize").hasArgs().withDescription("Message database connection pool size").create());
    options.addOption(withLongOpt("secure").hasArgs().withDescription("Set 'true' if you require all publishers and subscribers to be registered before accessing the system. " +
        "Set 'false' to allow any subscriber/publisher to connect. Default = true.").create());


    if (args.length == 0) {
      printHelpExit(options);
    }


    String dbUrl;
    String dbUser;
    String dbPass;
    int dbPoolSize;
    final SecurityMode securityMode;

    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("dbUrl")) {
        dbUrl = line.getOptionValue("dbUrl");
      } else {
        throw new RuntimeException("You forgot to specify a database URL");
      }

      if (line.hasOption("dbUser")) {
        dbUser = line.getOptionValue("dbUser");
      } else {
        throw new RuntimeException("You forgot to specify a database username");
      }

      if (line.hasOption("dbPass")) {
        dbPass = line.getOptionValue("dbPass");
      } else {
        throw new RuntimeException("You forgot to specify a database password");
      }

      if (line.hasOption("dbPoolSize")) {
        dbPoolSize = Integer.parseInt(line.getOptionValue("dbPoolSize"));
      } else {
        dbPoolSize = 8;
      }

      if (line.hasOption("secure") && !Boolean.parseBoolean(line.getOptionValue("secure"))) {
        securityMode = SecurityMode.INSECURE;
      } else {
        securityMode = SecurityMode.SECURE;
      }
    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    DistributedJdbcPool connFact = HikariSqlPool.make(dbUser, dbPass, dbUrl, dbPoolSize, dbPoolSize);

    port(5000);
    hostPublisherService("/publisher", connFact, securityMode);
  }

  public static void hostPublisherService(String basePath, DistributedJdbcPool connFact, SecurityMode securityMode) {
    if (!basePath.isEmpty() && !basePath.startsWith("/")) {
      throw new RuntimeException("The base path should either be empty, or start with a '/'. Actual path was: "+basePath);
    }

    Gson gson = GsonFactory.make();
    TimeService clock = new SystemTimeService();

    PublisherStore publisherStore = new PublisherStore(connFact, "publishers", gson, new TypeToken<PublisherData>(){}, clock);
    HalogenStorage<String> storageService = new HalogenStorageSql(connFact, "messages");



    exception(Exception.class, (e, request, response) -> {
      e.printStackTrace();
      response.status(500);
      response.body("HalogenStorageException: "+e.getMessage()+"\n");
    });

    post(basePath + "/publish", (req, res) -> {
      String reqBody = req.body();
      //System.out.println("reqBody: "+reqBody);

      WsPublicationBean publication = gson.fromJson(reqBody, WsPublicationBean.class);

      UUID publisherId = publication.getPublisherId();
      UUID secretKey = publication.getSecretKey();

      if (!isPublisherAllowed(publisherStore, securityMode, publisherId, secretKey)) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      // Set publisher ID and publication time to ensure consistency
      for (Message<String> message : publication.getMessages()) {
        message.getHeader().setPublisherId(publisherId);
        message.getHeader().setDate(new Date(clock.getNow()));
      }
      storageService.storeAll(publication.getMessages());
      res.status(202); // HTTP accepted
      return "Message accepted";
    });

    get(basePath + "/isRegistered/:publisher_id", (req, res) -> {
      UUID subscriberId = UUID.fromString(req.params().get(":publisher_id"));
      res.status(200);
      return gson.toJson(publisherStore.existsByUid(subscriberId));
    });


    post(basePath + "/listAllPublisherResponsesTo", (req, res) -> {
      String reqBody = req.body();
      WsListAllPublisherResponsesToRequest requestBean = gson.fromJson(reqBody, WsListAllPublisherResponsesToRequest.class);

      UUID publisherId = requestBean.getPublisherId();
      UUID secretKey = requestBean.getSecretKey();

      if (!isPublisherAllowed(publisherStore, securityMode, publisherId, secretKey)) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      res.status(202); // HTTP accepted
      return gson.toJson(storageService.listAllPublisherResponsesTo(publisherId,
          requestBean.getMessageId(), requestBean.getMaxMsgs(), requestBean.isAscending()));
    });


    post(basePath + "/lastMessagePublisherSent", (req, res) -> {
      String reqBody = req.body();
      WsLastMessageSentRequest requestBean = gson.fromJson(reqBody, WsLastMessageSentRequest.class);

      UUID publisherId = requestBean.getPublisherId();
      UUID secretKey = requestBean.getSecretKey();

      if (!isPublisherAllowed(publisherStore, securityMode, publisherId, secretKey)) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      res.status(202); // HTTP accepted
      return gson.toJson(storageService.lastMessagePublisherSent(publisherId));
    });


    post(basePath + "/lastMessagePublisherRespondedTo", (req, res) -> {
      String reqBody = req.body();
      WsLastMessageRespondedToRequest requestBean = gson.fromJson(reqBody, WsLastMessageRespondedToRequest.class);

      UUID publisherId = requestBean.getPublisherId();
      UUID secretKey = requestBean.getSecretKey();

      if (!isPublisherAllowed(publisherStore, securityMode, publisherId, secretKey)) {
        res.status(401); // Unauthorized
        return "Unauthorized";
      }

      res.status(202); // HTTP accepted
      return gson.toJson(storageService.lastMessagePublisherRespondedTo(publisherId));
    });
  }

  private static boolean isPublisherAllowed(PublisherStore publisherStore, SecurityMode policy,
                                            UUID pubId, UUID secretKey) throws ObjectStoreException, ObjectNotFoundException {
    if (policy == SecurityMode.INSECURE) {
      return true;
    }

    if (!publisherStore.existsByUid(pubId)) {
      return false;
    }

    PublisherData publisherData = publisherStore.getByUid(pubId);
    return secretKey.equals(publisherData.getSecretKey());
  }
}
