/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.publisher;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.store.HalogenStorageException;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public interface HalPublisher<T> {
  /**
   * Publishes a single message.
   *
   * Note that the message header's publisher ID and timestamp fields may be overwritten by the
   * publisher to ensure consistency. If you want to store a message instance "verbatim", then you
   * should use the <code>HalogenStorage</code> API.
   *
   * @param message the message to publish
   * @throws HalogenStorageException
   */
  void publish(Message<T> message) throws HalogenStorageException;
  void publishAll(List<Message<T>> messages) throws HalogenStorageException;


  boolean isRegistered() throws HalogenStorageException;

  List<Message<T>> listAllPublisherResponsesTo(UUID messageId, int maxMsgs, boolean ascending) throws HalogenStorageException;
  Message<T> lastMessagePublisherSent() throws HalogenStorageException;
  Message<T> lastMessagePublisherRespondedTo() throws HalogenStorageException;
}
