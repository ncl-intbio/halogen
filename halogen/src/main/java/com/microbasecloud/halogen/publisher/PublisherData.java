/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.microbasecloud.halogen.publisher;

import com.microbasecloud.fluorine.AbstractVersionableObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class PublisherData extends AbstractVersionableObject {

  private UUID secretKey;
  private boolean enabled; // Can be used to disable this publisher

  private boolean locked;  // Reserved for future implementations to temporarily lock this object for processing

  // Tags that can be used to store user-defined information about a publisher
  private Map<String, String> tags;

  public PublisherData() {
    this.enabled = true;
    this.locked = false;
    this.tags = new HashMap<>();
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  public UUID getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(UUID secretKey) {
    this.secretKey = secretKey;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public Map<String, String> getTags() {
    return tags;
  }

  public void setTags(Map<String, String> tags) {
    this.tags = tags;
  }

}
