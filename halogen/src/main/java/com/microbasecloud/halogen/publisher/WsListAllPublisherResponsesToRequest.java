/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.publisher;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class WsListAllPublisherResponsesToRequest {
  private UUID publisherId;
  private UUID secretKey;
  private UUID messageId;
  private int maxMsgs;
  private boolean ascending;

  public WsListAllPublisherResponsesToRequest() {
  }

  public WsListAllPublisherResponsesToRequest(UUID publisherId, UUID secretKey, UUID messageId, int maxMsgs, boolean ascending) {
    this.publisherId = publisherId;
    this.secretKey = secretKey;
    this.messageId = messageId;
    this.maxMsgs = maxMsgs;
    this.ascending = ascending;
  }

  public UUID getPublisherId() {
    return publisherId;
  }

  public void setPublisherId(UUID publisherId) {
    this.publisherId = publisherId;
  }

  public UUID getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(UUID secretKey) {
    this.secretKey = secretKey;
  }

  public UUID getMessageId() {
    return messageId;
  }

  public void setMessageId(UUID messageId) {
    this.messageId = messageId;
  }

  public int getMaxMsgs() {
    return maxMsgs;
  }

  public void setMaxMsgs(int maxMsgs) {
    this.maxMsgs = maxMsgs;
  }

  public boolean isAscending() {
    return ascending;
  }

  public void setAscending(boolean ascending) {
    this.ascending = ascending;
  }
}
