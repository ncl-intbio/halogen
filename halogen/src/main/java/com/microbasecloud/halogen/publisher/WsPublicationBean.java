/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.publisher;

import com.microbasecloud.halogen.Message;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class WsPublicationBean {
  private UUID publisherId;
  private UUID secretKey;
  private List<Message<String>> messages;

  public WsPublicationBean() {
  }

  public WsPublicationBean(UUID publisherId, UUID secretKey, List<Message<String>> messages) {
    this.publisherId = publisherId;
    this.secretKey = secretKey;
    this.messages = messages;
  }

  public WsPublicationBean(UUID publisherId, UUID secretKey, Message<String> message) {
    this(publisherId, secretKey, List.of(message));
  }

  public UUID getPublisherId() {
    return publisherId;
  }

  public void setPublisherId(UUID publisherId) {
    this.publisherId = publisherId;
  }

  public UUID getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(UUID secretKey) {
    this.secretKey = secretKey;
  }

  public List<Message<String>> getMessages() {
    return messages;
  }

  public void setMessages(List<Message<String>> messages) {
    this.messages = messages;
  }
}
