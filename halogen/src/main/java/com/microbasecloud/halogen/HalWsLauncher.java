/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen;

import com.scalesinformatics.dbpool.DistributedJdbcPool;
import com.scalesinformatics.dbpool.HikariSqlPool;
import org.apache.commons.cli.*;

import java.util.UUID;

import static com.microbasecloud.halogen.admin.HalAdminWsLauncher.hostAdminService;
import static com.microbasecloud.halogen.publisher.HalPublisherWsLauncher.hostPublisherService;
import static com.microbasecloud.halogen.store.HalogenStorageWs.hostStorageService;
import static com.microbasecloud.halogen.subscriber.HalSubscriberWsLauncher.hostSubscriberService;
import static org.apache.commons.cli.OptionBuilder.withLongOpt;
import static spark.Spark.port;

/**
 * @author Keith Flanagan
 */
public class HalWsLauncher {

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "HalWsLauncher.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("dbUrl").hasArgs().withDescription("Admin database URL (e.g.: jdbc:postgresql://hostname:port/dbname)").create());
    options.addOption(withLongOpt("dbUser").hasArgs().withDescription("Admin database username").create());
    options.addOption(withLongOpt("dbPass").hasArgs().withDescription("Admin database password").create());
    options.addOption(withLongOpt("dbPoolSize").hasArgs().withDescription("Admin database connection pool size").create());
    options.addOption(withLongOpt("secure").hasArgs().withDescription("Set 'true' if you require all operations to be authenticated. " +
        "Set 'false' to allow any user to connect and interact with this service. Default = true.").create());
    options.addOption(withLongOpt("bootstrap-admin").hasArgs().withDescription("[Optional] Allows specifying an initial admin 'user'. " +
        "You should specify two UUIDs: the initial admin ID, and the associated 'secret' key. " +
        "This user is not stored in the database.").create());

    options.addOption(withLongOpt("admin").withDescription("Host admin service").create());
    options.addOption(withLongOpt("store").withDescription("Host message store service").create());
    options.addOption(withLongOpt("publisher").withDescription("Host publisher service").create());
    options.addOption(withLongOpt("subscriber").withDescription("Host subscriber service").create());

    if (args.length == 0) {
      printHelpExit(options);
    }


    String dbUrl;
    String dbUser;
    String dbPass;
    int dbPoolSize;
    final SecurityMode securityMode;

    final UUID bootstrapAdminId;
    final UUID bootstrapAdminSecret;

    boolean adminService;
    boolean storeService;
    boolean publisherService;
    boolean subscriberService;

    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("dbUrl")) {
        dbUrl = line.getOptionValue("dbUrl");
      } else {
        throw new RuntimeException("You forgot to specify a database URL");
      }

      if (line.hasOption("dbUser")) {
        dbUser = line.getOptionValue("dbUser");
      } else {
        throw new RuntimeException("You forgot to specify a database username");
      }

      if (line.hasOption("dbPass")) {
        dbPass = line.getOptionValue("dbPass");
      } else {
        throw new RuntimeException("You forgot to specify a database password");
      }

      if (line.hasOption("dbPoolSize")) {
        dbPoolSize = Integer.parseInt(line.getOptionValue("dbPoolSize"));
      } else {
        dbPoolSize = 8;
      }

      if (line.hasOption("secure") && !Boolean.parseBoolean(line.getOptionValue("secure"))) {
        securityMode = SecurityMode.INSECURE;
      } else {
        securityMode = SecurityMode.SECURE;
      }

      if (line.hasOption("bootstrap-admin")) {
        String[] adminStrings = line.getOptionValues("bootstrap-admin");
        bootstrapAdminId = UUID.fromString(adminStrings[0]);
        bootstrapAdminSecret = UUID.fromString(adminStrings[1]);
      } else {
        bootstrapAdminId = null;
        bootstrapAdminSecret = null;
      }


      adminService = line.hasOption("admin");
      storeService = line.hasOption("store");
      publisherService = line.hasOption("publisher");
      subscriberService = line.hasOption("subscriber");
    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    DistributedJdbcPool connFact = HikariSqlPool.make(dbUser, dbPass, dbUrl, dbPoolSize, dbPoolSize);

    port(5000);

    if (adminService) {
      hostAdminService("/admin", connFact, securityMode, bootstrapAdminId, bootstrapAdminSecret);
    }

    if (storeService) {
      hostStorageService("/storage", connFact);
    }

    if (publisherService) {
      hostPublisherService("/publisher", connFact, securityMode);
    }

    if (subscriberService) {
      hostSubscriberService("/subscriber", connFact, securityMode);
    }

  }

}
