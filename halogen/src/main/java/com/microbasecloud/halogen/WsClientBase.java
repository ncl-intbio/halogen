package com.microbasecloud.halogen;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.store.HalogenStorageException;
import com.microbasecloud.util.GsonFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Logger;

/**
 * @author James McLaughlin
 * @author Keith Flanagan
 */
public class WsClientBase {
  private static final Logger logger = Logger.getLogger(WsClientBase.class.getName());

  protected PoolingHttpClientConnectionManager connectionManager;
  protected HttpClient client;
  protected String wsUrl;
  protected Gson gson;

  public WsClientBase(Gson gson, String wsUrl) {

    this.wsUrl = wsUrl;

    //gson = GsonFactory.make();
    this.gson = gson;

    connectionManager = new PoolingHttpClientConnectionManager();
    connectionManager.setMaxTotal(500);
    // Increase default max connection per route to 500
    connectionManager.setDefaultMaxPerRoute(500);
    client = HttpClients.custom().setConnectionManager(connectionManager).build();

  }

  protected String encodeURIComponent(String str) throws WsClientException {
    try {
      return URLEncoder.encode(str, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw new WsClientException(e);
    }
  }

  protected String consumeStringResponse(HttpResponse res) throws IOException {
    try {
      checkResponseCode(res);
      return stringFromResponse(res);
    } finally {
      EntityUtils.consume(res.getEntity());
    }
  }

  protected <T> T consumeResponse(HttpResponse res, TypeToken<T> typeToken) throws IOException {
    try {
      checkResponseCode(res);
      return objectFromResponse(res, typeToken);
    } finally {
      EntityUtils.consume(res.getEntity());
    }
  }


  protected void checkResponseCode(HttpResponse response) throws WsClientException {
    int statusCode = response.getStatusLine().getStatusCode();

    if (statusCode >= 300) {
      throw new WsClientException("HTTP " + statusCode);
    }
  }

  protected String stringFromResponse(HttpResponse res) {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      res.getEntity().writeTo(baos);
      return new String(baos.toByteArray());
    } catch (Exception e) {
      throw new RuntimeException("Failed to extract a string from a HttpResponse", e);
    }
  }

  protected <T> T objectFromResponse(HttpResponse res, TypeToken<T> typeToken) {
    String jsonText = stringFromResponse(res);
    //logger.info("DEBUG. HttpResponse was: "+jsonText);
    try {
      return gson.fromJson(jsonText, typeToken.getType());
    } catch (Exception e) {
      throw new RuntimeException("Failed to extract an object from a HTTP response. " +
          "Expected type was: " + typeToken + ". JSON text was: " + jsonText, e);
    }
  }
}
