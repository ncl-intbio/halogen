/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.store;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.util.GsonFactory;
import com.scalesinformatics.dbpool.DistributedJdbcPool;
import com.scalesinformatics.dbpool.HikariSqlPool;
import org.apache.commons.cli.*;

import java.util.List;
import java.util.UUID;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;
import static spark.Spark.*;

/**
 * @author Keith Flanagan
 */
public class HalogenStorageWs implements HalogenStorage<String> {
  private HalogenStorage<String> db;


  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "HalogenStorageWs.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("dbUrl").hasArgs().withDescription("Message database URL (e.g.: jdbc:postgresql://hostname:port/dbname)").create());
    options.addOption(withLongOpt("dbUser").hasArgs().withDescription("Message database username").create());
    options.addOption(withLongOpt("dbPass").hasArgs().withDescription("Message database password").create());
    options.addOption(withLongOpt("dbPoolSize").hasArgs().withDescription("Message database connection pool size").create());


    if (args.length == 0) {
      printHelpExit(options);
    }


    String dbUrl;
    String dbUser;
    String dbPass;
    int dbPoolSize;

    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("dbUrl")) {
        dbUrl = line.getOptionValue("dbUrl");
      } else {
        throw new RuntimeException("You forgot to specify a database URL");
      }

      if (line.hasOption("dbUser")) {
        dbUser = line.getOptionValue("dbUser");
      } else {
        throw new RuntimeException("You forgot to specify a database username");
      }

      if (line.hasOption("dbPass")) {
        dbPass = line.getOptionValue("dbPass");
      } else {
        throw new RuntimeException("You forgot to specify a database password");
      }

      if (line.hasOption("dbPoolSize")) {
        dbPoolSize = Integer.parseInt(line.getOptionValue("dbPoolSize"));
      } else {
        dbPoolSize = 8;
      }
    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    DistributedJdbcPool connFact = HikariSqlPool.make(dbUser, dbPass, dbUrl, dbPoolSize, dbPoolSize);

    port(5000);
    hostStorageService("/storage", connFact);
  }

  public static void hostStorageService(String basePath, DistributedJdbcPool connFact) {
    if (!basePath.isEmpty() && !basePath.startsWith("/")) {
      throw new RuntimeException("The base path should either be empty, or start with a '/'. Actual path was: "+basePath);
    }

    HalogenStorageWs ws = new HalogenStorageWs(new HalogenStorageSql(connFact, "messages"));

    Gson gson = GsonFactory.make();

    exception(IllegalArgumentException.class, (e, request, response) -> {
      response.status(400);
      response.body("IllegalArgumentException: "+e.getMessage()+"\n");
    });

    exception(ObjectNotFoundException.class, (e, request, response) -> {
      response.status(400);
      response.body("ObjectNotFoundException: "+e.getMessage()+"\n");
    });

    exception(HalogenStorageException.class, (e, request, response) -> {
      response.status(400);
      response.body("HalogenStorageException: "+e.getMessage()+"\n");
    });


    post(basePath + "/store", (req, res) -> {
      Message<String> message = gson.fromJson(req.body(), new TypeToken<Message<String>>(){}.getType());
      ws.store(message);
      res.status(202); // HTTP accepted
      return "Message accepted";
    });

    post(basePath + "/storeAll", (req, res) -> {
      List<Message<String>> messages = gson.fromJson(req.body(), new TypeToken<List<Message<String>>>(){}.getType());
      ws.storeAll(messages);
      res.status(202); // HTTP accepted
      return "Message accepted";
    });


    get(basePath + "/getByUid/:msg_uid", (req, res) -> {
      UUID msgUid = UUID.fromString(req.params().get(":msg_uid"));
      return gson.toJson(ws.getByUid(msgUid));
    });

    get(basePath + "/first", (req, res) -> {
      return gson.toJson(ws.first());
    });

    get(basePath + "/last", (req, res) -> {
      return gson.toJson(ws.last());
    });

    get(basePath + "/listAfter/:after_msg_uid/:max_msgs/:ascending", (req, res) -> {
      UUID afterMsgUid = UUID.fromString(req.params().get(":after_msg_uid"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listAfter(afterMsgUid, maxMsgs, ascending));
    });

    get(basePath + "/first/:channel", (req, res) -> {
      String channel = req.params().get(":channel");
      return gson.toJson(ws.first(channel));
    });

    get(basePath + "/last/:channel", (req, res) -> {
      String channel = req.params().get(":channel");
      return gson.toJson(ws.last(channel));
    });

    get(basePath + "/listByChannel/:channel/:after_msg_uid/:max_msgs/:ascending", (req, res) -> {
      String channel = req.params().get(":channel");
      UUID afterMsgUid = UUID.fromString(req.params().get(":after_msg_uid"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listByChannel(channel, afterMsgUid, maxMsgs, ascending));
    });

    get(basePath + "/first/:channel/:type", (req, res) -> {
      String channel = req.params().get(":channel");
      String type = req.params().get(":type");
      return gson.toJson(ws.first(channel, type));
    });

    get(basePath + "/last/:channel/:type", (req, res) -> {
      String channel = req.params().get(":channel");
      String type = req.params().get(":type");
      return gson.toJson(ws.last(channel, type));
    });

    get(basePath + "/listByChannelAndType/:channel/:type/:after_msg_uid/:max_msgs/:ascending", (req, res) -> {
      String channel = req.params().get(":channel");
      String type = req.params().get(":type");
      UUID afterMsgUid = UUID.fromString(req.params().get(":after_msg_uid"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listByChannelAndType(channel, type, afterMsgUid, maxMsgs, ascending));
    });



    get(basePath + "/firstByChannelAndTopic/:channel/:topic", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      return gson.toJson(ws.firstByChannelAndTopic(channel, topic));
    });

    get(basePath + "/lastByChannelAndTopic/:channel/:topic", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      return gson.toJson(ws.lastByChannelAndTopic(channel, topic));
    });

    get(basePath + "/listByChannelAndTopic/:channel/:topic", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      return gson.toJson(ws.listByChannelAndTopic(channel, topic));
    });

    get(basePath + "/listByChannelAndTopic/:channel/:topic/:after_msg_uid/:max_msgs/:ascending", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      UUID afterMsgUid = UUID.fromString(req.params().get(":after_msg_uid"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listByChannelAndTopic(channel, topic, afterMsgUid, maxMsgs, ascending));
    });

    get(basePath + "/firstByChannelTopicType/:channel/:topic/:type", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      String type = req.params().get(":type");
      return gson.toJson(ws.firstByChannelTopicType(channel, topic, type));
    });

    get(basePath + "/lastByChannelTopicType/:channel/:topic/:type", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      String type = req.params().get(":type");
      return gson.toJson(ws.lastByChannelTopicType(channel, topic, type));
    });

    get(basePath + "/listByChannelTopicType/:channel/:topic/:type", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      String type = req.params().get(":type");
      return gson.toJson(ws.listByChannelTopicType(channel, topic, type));
    });

    get(basePath + "/listByChannelTopicType/:channel/:topic/:type/:after_msg_uid/:max_msgs/:ascending", (req, res) -> {
      String channel = req.params().get(":channel");
      String topic = req.params().get(":topic");
      String type = req.params().get(":type");
      UUID afterMsgUid = UUID.fromString(req.params().get(":after_msg_uid"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listByChannelTopicType(channel, topic, type, afterMsgUid, maxMsgs, ascending));
    });

    get(basePath + "/firstByType/:type", (req, res) -> {
      String type = req.params().get(":type");
      return gson.toJson(ws.firstByType(type));
    });

    get(basePath + "/lastByType/:type", (req, res) -> {
      String type = req.params().get(":type");
      return gson.toJson(ws.lastByType(type));
    });

    get(basePath + "/listByType/:type/:after_msg_uid/:max_msgs/:ascending", (req, res) -> {
      String type = req.params().get(":type");
      UUID afterMsgUid = UUID.fromString(req.params().get(":after_msg_uid"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listByType(type, afterMsgUid, maxMsgs, ascending));
    });


    get(basePath + "/listAllPublisherResponsesTo/:publisher_uid/:responses_to_msg/:max_msgs/:ascending", (req, res) -> {
      UUID publisherUid = UUID.fromString(req.params().get(":publisher_uid"));
      UUID responsesToMsg = UUID.fromString(req.params().get(":responses_to_msg"));
      int maxMsgs = parseInt(req.params().get(":max_msgs"), 1, 5000);
      boolean ascending = Boolean.parseBoolean(req.params().get(":ascending"));
      return gson.toJson(ws.listAllPublisherResponsesTo(publisherUid, responsesToMsg, maxMsgs, ascending));
    });

    get(basePath + "/lastMessagePublisherSent/:publisher_uid", (req, res) -> {
      UUID publisherUid = UUID.fromString(req.params().get(":publisher_uid"));
      return gson.toJson(ws.lastMessagePublisherSent(publisherUid));
    });

    get(basePath + "/lastMessagePublisherRespondedTo/:publisher_uid", (req, res) -> {
      UUID publisherUid = UUID.fromString(req.params().get(":publisher_uid"));
      return gson.toJson(ws.lastMessagePublisherRespondedTo(publisherUid));
    });
  }

  private static int parseInt(String intString, int minIncl, int maxIncl) throws IllegalArgumentException {
    int val;
    try {
      val = Integer.parseInt(intString);
    } catch (Exception e) {
      throw new IllegalArgumentException("Input was not a valid integer: "+intString, e);
    }

    if (val < minIncl || val > maxIncl) {
      throw new IllegalArgumentException("Value "+intString+" was outside valid range: "+minIncl+" -- "+maxIncl);
    }
    return val;
  }


  public HalogenStorageWs(HalogenStorage<String> db) {
    this.db = db;
  }


  @Override
  public void store(Message<String> message) throws HalogenStorageException {
    db.store(message);
  }

  @Override
  public void storeAll(List<Message<String>> messages) throws HalogenStorageException {
    db.storeAll(messages);
  }

  @Override
  public Message<String> getByUid(UUID msgUid) throws HalogenStorageException {
    return db.getByUid(msgUid);
  }

  @Override
  public Message<String> first() throws HalogenStorageException {
    return db.first();
  }

  @Override
  public Message<String> last() throws HalogenStorageException {
    return db.last();
  }

  @Override
  public List<Message<String>> listAfter(UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return db.listAfter(afterMsgUid, maxMsgs, ascending);
  }

  @Override
  public Message<String> first(String channel) throws HalogenStorageException {
    return db.first(channel);
  }

  @Override
  public Message<String> last(String channel) throws HalogenStorageException {
    return db.last(channel);
  }

  @Override
  public List<Message<String>> listByChannel(String channel, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return db.listByChannel(channel, afterMsgUid, maxMsgs, ascending);
  }

  @Override
  public Message<String> first(String channel, String type) throws HalogenStorageException {
    return db.first(channel, type);
  }

  @Override
  public Message<String> last(String channel, String type) throws HalogenStorageException {
    return db.last(channel, type);
  }

  @Override
  public List<Message<String>> listByChannelAndType(String channel, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return db.listByChannelAndType(channel, type, afterMsgUid, maxMsgs, ascending);
  }

  @Override
  public Message<String> firstByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    return db.firstByChannelAndTopic(channel, topic);
  }

  @Override
  public Message<String> lastByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    return db.lastByChannelAndTopic(channel, topic);
  }

  @Override
  public List<Message<String>> listByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    return db.listByChannelAndTopic(channel, topic);
  }

  @Override
  public List<Message<String>> listByChannelAndTopic(String channel, String topic, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return db.listByChannelAndTopic(channel, topic, afterMsgUid, maxMsgs, ascending);
  }

  @Override
  public Message<String> firstByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    return db.firstByChannelTopicType(channel, topic, type);
  }

  @Override
  public Message<String> lastByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    return db.lastByChannelTopicType(channel, topic, type);
  }

  @Override
  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    return db.listByChannelTopicType(channel, topic, type);
  }

  @Override
  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return db.listByChannelTopicType(channel, topic, type, afterMsgUid, maxMsgs, ascending);
  }

  @Override
  public Message<String> firstByType(String type) throws HalogenStorageException {
    return db.firstByType(type);
  }

  @Override
  public Message<String> lastByType(String type) throws HalogenStorageException {
    return db.lastByType(type);
  }

  @Override
  public List<Message<String>> listByType(String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return listByType(type, afterMsgUid, maxMsgs, ascending);
  }

  @Override
  public List<Message<String>> listAllPublisherResponsesTo(UUID publisherUid, UUID responsesToMsg, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return db.listAllPublisherResponsesTo(publisherUid, responsesToMsg, maxMsgs, ascending);
  }

  @Override
  public Message<String> lastMessagePublisherSent(UUID publisherUid) throws HalogenStorageException {
    return db.lastMessagePublisherSent(publisherUid);
  }

  @Override
  public Message<String> lastMessagePublisherRespondedTo(UUID publisherUid) throws HalogenStorageException {
    return db.lastMessagePublisherRespondedTo(publisherUid);
  }
}
