/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.store;

import com.microbasecloud.halogen.Message;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public interface HalogenStorage<T> {
  void store(Message<T> message) throws HalogenStorageException;
  void storeAll(List<Message<T>> messages) throws HalogenStorageException;

  Message<T> getByUid(UUID msgUid) throws HalogenStorageException;

  Message<T> first() throws HalogenStorageException;
  Message<T> last() throws HalogenStorageException;
  List<Message<T>> listAfter(UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException;

  Message<T> first(String channel) throws HalogenStorageException;
  Message<T> last(String channel) throws HalogenStorageException;
  List<Message<T>> listByChannel(String channel, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException;


  Message<T> first(String channel, String type) throws HalogenStorageException;
  Message<T> last(String channel, String type) throws HalogenStorageException;
  List<Message<T>> listByChannelAndType(String channel, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException;


  Message<T> firstByChannelAndTopic(String channel, String topic) throws HalogenStorageException;
  Message<T> lastByChannelAndTopic(String channel, String topic) throws HalogenStorageException;
  List<Message<T>> listByChannelAndTopic(String channel, String topic) throws HalogenStorageException;
  List<Message<T>> listByChannelAndTopic(String channel, String topic, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException;

  Message<T> firstByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException;
  Message<T> lastByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException;
  List<Message<T>> listByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException;
  List<Message<T>> listByChannelTopicType(String channel, String topic, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException;

  Message<T> firstByType(String type) throws HalogenStorageException;
  Message<T> lastByType(String type) throws HalogenStorageException;
  List<Message<T>> listByType(String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException;

  List<Message<T>> listAllPublisherResponsesTo(UUID publisherUid, UUID responsesToMsg, int maxMsgs, boolean ascending) throws HalogenStorageException;

  Message<T> lastMessagePublisherSent(UUID publisherUid) throws HalogenStorageException;
  Message<T> lastMessagePublisherRespondedTo(UUID publisherUid) throws HalogenStorageException;

}
