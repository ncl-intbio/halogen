package com.microbasecloud.halogen.store;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.WsClientBase;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author James Alastair McLaughlin <j.a.mclaughlin@ncl.ac.uk>
 */
public class HalogenStorageWsClient extends WsClientBase implements HalogenStorage<String> {

  public HalogenStorageWsClient(Gson gson, String wsUrl) {

    super(gson, wsUrl);

  }

  @Override
  public void store(Message<String> message) throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/storage/store");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(message)));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public void storeAll(List<Message<String>> messages) throws HalogenStorageException {
    HttpPost req = new HttpPost(wsUrl + "/storage/storeAll");
    try {
      req.setHeader("Content-Type", "application/json");
      req.setEntity(new StringEntity(gson.toJson(messages)));

      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> getByUid(UUID msgUid) throws HalogenStorageException {

    HttpGet req = new HttpGet(wsUrl + "/storage/getByUid"
        + "/" + msgUid.toString());

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public Message<String> first() throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/first");

    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> last() throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/last");

    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listAfter(UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {

    HttpGet req = new HttpGet(wsUrl + "/storage/listAfter"
        + "/" + afterMsgUid.toString()
        + "/" + maxMsgs
        + "/" + ascending);

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public Message<String> first(String channel) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/first"
        + "/" + encodeURIComponent(channel));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> last(String channel) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/last"
        + "/" + encodeURIComponent(channel));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listByChannel(String channel, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/listByChannel"
        + "/" + encodeURIComponent(channel)
        + "/" + afterMsgUid.toString()
        + "/" + maxMsgs
        + "/" + ascending);

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public Message<String> first(String channel, String type) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/first"
        + "/" + encodeURIComponent(channel)
        + "/" + encodeURIComponent(type));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> last(String channel, String type) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/last"
        + "/" + encodeURIComponent(channel)
        + "/" + encodeURIComponent(type));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listByChannelAndType(String channel, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {

    HttpGet req = new HttpGet(
        wsUrl + "/storage/listByChannelAndType"
            + "/" + encodeURIComponent(channel)
            + "/" + encodeURIComponent(type)
            + "/" + afterMsgUid.toString()
            + "/" + maxMsgs
            + "/" + ascending);

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public Message<String> firstByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/firstByChannelAndTopic"
            + "/" + encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/lastByChannelAndTopic"
            + "/" + encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listByChannelAndTopic(String channel, String topic) throws HalogenStorageException {

    HttpGet req = new HttpGet(
        wsUrl + "/storage/listByChannelAndTopic"
            + "/" + encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic));

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public List<Message<String>> listByChannelAndTopic(String channel, String topic, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {

    HttpGet req = new HttpGet(
        wsUrl + "/storage/listByChannelAndTopic"
            + "/" + encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic)
            + "/" + afterMsgUid.toString()
            + "/" + maxMsgs
            + "/" + ascending);

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public Message<String> firstByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/firstByChannelTopicType"
            + "/" +  encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic)
            + "/" + encodeURIComponent(type));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/lastByChannelTopicType"
            + "/" + encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic)
            + "/" + encodeURIComponent(type));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {

    HttpGet req = new HttpGet(
        wsUrl + "/storage/listByChannelTopicType"
            + "/" +  encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic)
            + "/" + encodeURIComponent(type));
    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {

    HttpGet req = new HttpGet(
        wsUrl + "/storage/listByChannelTopicType/"
            + encodeURIComponent(channel)
            + "/" + encodeURIComponent(topic)
            + "/" + encodeURIComponent(type)
            + "/" + afterMsgUid.toString()
            + "/" + maxMsgs
            + "/" + ascending);

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }

  }

  @Override
  public Message<String> firstByType(String type) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/firstByType"
        + "/" + encodeURIComponent(type)
        + "/" + encodeURIComponent(type));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastByType(String type) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/lastByType"
        + "/" + encodeURIComponent(type));
    try {
      HttpResponse res = client.execute(req);
      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public List<Message<String>> listByType(String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    HttpGet req = new HttpGet(wsUrl + "/storage/listByType"
        + "/" + encodeURIComponent(type)
        + "/" + afterMsgUid.toString()
        + "/" + maxMsgs
        + "/" + ascending);

    try {

      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }

    } catch (IOException e) {

      throw new HalogenStorageException(e);

    }
  }

  @Override
  public List<Message<String>> listAllPublisherResponsesTo(UUID publisherUid, UUID responsesToMsg, int maxMsgs, boolean ascending) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/listAllPublisherResponsesTo"
            + "/" + publisherUid.toString()
            + "/" + responsesToMsg.toString()
            + "/" + maxMsgs
            + "/" + ascending);

    try {
      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messagesFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastMessagePublisherSent(UUID publisherUid) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/lastMessagePublisherSent"
            + "/" + publisherUid.toString());

    try {
      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }

  @Override
  public Message<String> lastMessagePublisherRespondedTo(UUID publisherUid) throws HalogenStorageException {
    HttpGet req = new HttpGet(
        wsUrl + "/storage/lastMessagePublisherRespondedTo"
            + "/" + publisherUid.toString());

    try {
      HttpResponse res = client.execute(req);

      try {
        checkResponseCode(res);
        return messageFromResponse(res);
      } finally {
        EntityUtils.consume(res.getEntity());
      }
    } catch (IOException e) {
      throw new HalogenStorageException(e);
    }
  }


  private Message<String> messageFromResponse(HttpResponse res) {
    return objectFromResponse(res, new TypeToken<Message<String>>() {});
  }

  private List<Message<String>> messagesFromResponse(HttpResponse res) {
    return objectFromResponse(res, new TypeToken<List<Message<String>>>() {});
  }

}
