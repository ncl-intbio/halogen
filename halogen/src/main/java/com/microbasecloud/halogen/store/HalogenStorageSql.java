/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.store;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.wrappers.BasicMessage;
import com.scalesinformatics.dbpool.DistributedJdbcPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HalogenStorageSql implements HalogenStorage<String> {
  protected final DistributedJdbcPool connFact;
  protected final String table;

  public HalogenStorageSql(DistributedJdbcPool connFact, String table) {
    this.connFact = connFact;
    this.table = table;
  }

  private long queryLocalIdForUid(Connection conn, UUID uid) throws HalogenStorageException {
    try (PreparedStatement stmt = conn.prepareStatement(String.format(
        "SELECT id FROM %s WHERE uid = ?", table))) {
      stmt.setObject(1, uid);

      ResultSet rs = stmt.executeQuery();
      if (!rs.next()) {
        throw new ObjectNotFoundException("Failed to find message: "+uid.toString());
      }
      return rs.getLong("id");
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  private Message<String> rowToMessage(ResultSet rs) throws SQLException {
    Message<String> message = new BasicMessage<>();
    message.getHeader().setUid((UUID) rs.getObject("uid"));
    message.getHeader().setParentId((UUID) rs.getObject("parent_uid"));
    message.getHeader().setPublisherId((UUID) rs.getObject("publisher"));
    message.getHeader().setChannel(rs.getString("channel"));

    message.getHeader().setDate(new Date(rs.getTimestamp("date").getTime()));

    message.getHeader().setTopic(rs.getString("topic"));
    message.getHeader().setType(rs.getString("type"));

    message.setContent(rs.getString("payload"));

    return message;
  }

  private List<Message<String>> rowsToList(ResultSet rs) throws SQLException {
    List<Message<String>> objects = new ArrayList<>();
    while (rs.next()) {
      objects.add(rowToMessage(rs));
    }
    return objects;
  }


  @Override
  public void store(Message<String> message) throws HalogenStorageException {
    try (Connection conn = connFact.getWritableConnection();
         PreparedStatement insert =
             conn.prepareStatement(String.format(
                 "INSERT INTO %s (" +
                     "uid, parent_uid, publisher, channel, date, " +
                     "topic, type, payload" +
                     ") VALUES (?,?,?,?,?,?,?,?)", table))) {

      int col = 1;
      insert.setObject(col++, message.getHeader().getUid());
      insert.setObject(col++, message.getHeader().getParentId());

      insert.setObject(col++, message.getHeader().getPublisherId());
      insert.setString(col++, message.getHeader().getChannel());

      insert.setTimestamp(col++, new Timestamp(message.getHeader().getDate().getTime()));
      insert.setString(col++, message.getHeader().getTopic());
      insert.setString(col++, message.getHeader().getType());

      insert.setString(col++, message.getContent());

      insert.execute();
      conn.commit();
    } catch (Exception e) {
      throw new HalogenStorageException("Failed to store message: " + message.getHeader().toString(), e);
    }
  }

  @Override
  public void storeAll(List<Message<String>> messages) throws HalogenStorageException {
    try (Connection conn = connFact.getWritableConnection();
         PreparedStatement insert =
             conn.prepareStatement(String.format(
                 "INSERT INTO %s (" +
                     "uid, parent_uid, publisher, channel, date, " +
                     "topic, type, payload" +
                     ") VALUES (?,?,?,?,?,?,?,?)", table))) {

      for (Message<String> message : messages) {
        int col = 1;
        insert.setObject(col++, message.getHeader().getUid());
        insert.setObject(col++, message.getHeader().getParentId());

        insert.setObject(col++, message.getHeader().getPublisherId());
        insert.setString(col++, message.getHeader().getChannel());

        insert.setTimestamp(col++, new Timestamp(message.getHeader().getDate().getTime()));
        insert.setString(col++, message.getHeader().getTopic());
        insert.setString(col++, message.getHeader().getType());

        insert.setString(col++, message.getContent());

        insert.addBatch();
      }
      insert.executeBatch();
      conn.commit();
    } catch (Exception e) {
      throw new HalogenStorageException(String.format("Failed to store %s messages", messages.size()), e);
    }
  }

  @Override
  public Message<String> getByUid(UUID msgUid) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection()) {
      return getByUid(conn, msgUid);
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  private Message<String> getByUid(Connection conn, UUID msgUid) throws HalogenStorageException {
    try (PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE uid = ?", table))) {
      stmt.setObject(1, msgUid);
      try (ResultSet rs = stmt.executeQuery()) {
        if (!rs.next()) {
          throw new ObjectNotFoundException("Failed to find message: " + msgUid.toString());
        }
        return rowToMessage(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }


  @Override
  public Message<String> first() throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s ORDER BY id LIMIT 1", table))) {
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> last() throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s ORDER BY id DESC LIMIT 1", table))) {
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listAfter(UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE id > ? ORDER BY id %s LIMIT ?",
             table, ascending ? "ASC" : "DESC")
         )) {
      stmt.setLong(1, queryLocalIdForUid(conn, afterMsgUid));
      stmt.setInt(2, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> first(String channel) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? ORDER BY id LIMIT 1", table))) {
      stmt.setString(1, channel);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> last(String channel) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? ORDER BY id DESC LIMIT 1", table))) {
      stmt.setString(1, channel);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listByChannel(String channel, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE id > ? AND channel = ? ORDER BY id %s LIMIT ?", table, ascending ? "ASC" : "DESC"))) {
      stmt.setLong(1, queryLocalIdForUid(conn, afterMsgUid));
      stmt.setString(2, channel);
      stmt.setInt(3, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> first(String channel, String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND type = ? ORDER BY id LIMIT 1", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> last(String channel, String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND type = ? ORDER BY id DESC LIMIT 1", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listByChannelAndType(String channel, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE id > ? AND channel = ? AND type = ? ORDER BY id %s LIMIT ?", table, ascending ? "ASC" : "DESC"))) {
      stmt.setLong(1, queryLocalIdForUid(conn, afterMsgUid));
      stmt.setString(2, channel);
      stmt.setString(3, type);
      stmt.setInt(4, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> firstByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND topic = ? ORDER BY id LIMIT 1", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, topic);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> lastByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND topic = ? ORDER BY id DESC LIMIT 1", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, topic);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listByChannelAndTopic(String channel, String topic) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND topic = ? ORDER BY id", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, topic);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listByChannelAndTopic(String channel, String topic, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE id > ? AND channel = ? AND topic = ? ORDER BY id %s LIMIT ?", table, ascending ? "ASC" : "DESC"))) {
      stmt.setLong(1, queryLocalIdForUid(conn, afterMsgUid));
      stmt.setString(2, channel);
      stmt.setString(3, topic);
      stmt.setInt(4, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> firstByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND topic = ? AND type = ? ORDER BY id LIMIT 1", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, topic);
      stmt.setString(3, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> lastByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND topic = ? AND type = ? ORDER BY id DESC LIMIT 1", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, topic);
      stmt.setString(3, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE channel = ? AND topic = ? AND type = ? ORDER BY id", table))) {
      stmt.setString(1, channel);
      stmt.setString(2, topic);
      stmt.setString(3, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

//  @Override
//  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type, UUID afterMsgUid, int maxMsgs) throws HalogenStorageException {
//  }

  @Override
  public List<Message<String>> listByChannelTopicType(String channel, String topic, String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE id > ? AND channel = ? AND topic = ? AND type = ? ORDER BY id %s LIMIT ?",
             table, ascending ? "ASC" : "DESC"))) {
      stmt.setLong(1, queryLocalIdForUid(conn, afterMsgUid));
      stmt.setString(2, channel);
      stmt.setString(3, topic);
      stmt.setString(4, type);
      stmt.setInt(5, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> firstByType(String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE type = ? ORDER BY id LIMIT 1", table))) {
      stmt.setString(1, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> lastByType(String type) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE type = ? ORDER BY id DESC LIMIT 1", table))) {
      stmt.setString(1, type);
      try (ResultSet rs = stmt.executeQuery()) {
        return rs.next() ? rowToMessage(rs) : null;
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listByType(String type, UUID afterMsgUid, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE id > ? AND type = ? ORDER BY id %s LIMIT ?", table, ascending ? "ASC" : "DESC"))) {
      stmt.setLong(1, queryLocalIdForUid(conn, afterMsgUid));
      stmt.setString(2, type);
      stmt.setInt(3, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public List<Message<String>> listAllPublisherResponsesTo(UUID publisherUid, UUID responsesToMsg, int maxMsgs, boolean ascending) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE publisher = ? AND parent_uid = ? ORDER BY id %s LIMIT ?",
             table, ascending ? "ASC" : "DESC"))) {
      int col = 1;
      stmt.setObject(col++, publisherUid);
      stmt.setObject(col++, responsesToMsg);
      stmt.setInt(col++, maxMsgs);
      try (ResultSet rs = stmt.executeQuery()) {
        return rowsToList(rs);
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> lastMessagePublisherSent(UUID publisherUid) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE publisher = ? ORDER BY id DESC LIMIT 1", table))) {
      int col = 1;
      stmt.setObject(col++, publisherUid);
      try (ResultSet rs = stmt.executeQuery()) {
        if (rs.next()) {
          return rowToMessage(rs);
        } else {
          return null;
        }
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }

  @Override
  public Message<String> lastMessagePublisherRespondedTo(UUID publisherUid) throws HalogenStorageException {
    try (Connection conn = connFact.getReadOnlyConnection();
         PreparedStatement stmt = conn.prepareStatement(String.format(
             "SELECT * FROM %s WHERE publisher = ? AND parent_uid NOT NULL ORDER BY id DESC LIMIT 1", table))) {
      int col = 1;
      stmt.setObject(col++, publisherUid);
      try (ResultSet rs = stmt.executeQuery()) {
        if (rs.next()) {
          return rowToMessage(rs);
        } else {
          return null;
        }
      }
    } catch (SQLException e) {
      throw new HalogenStorageException("Failed to query the database", e);
    }
  }
}
