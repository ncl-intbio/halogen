/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.helloworld;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.HalSubscriberWsClient;
import com.microbasecloud.util.GsonFactory;
import com.scalesinformatics.util.concurrent.ThreadUtils;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HelloHalogenSubscriber {
  public static void main(String[] args) {
    UUID subscriberId = UUID.fromString("00000000-0000-0000-0000-000000000001");
    UUID secretKey = UUID.fromString("00000000-0000-0000-0000-000000000001");
    HalSubscriber<String> subscriber = new HalSubscriberWsClient(GsonFactory.make(), "http://localhost:5000", subscriberId, secretKey);

    // TODO move these 3 lines to a HelloAdmin example
//    Subscription entireChannel = new Subscription();
//    entireChannel.setSubscriptionType(Subscription.Type.CHANNEL);
//    entireChannel.setChannel("channel1");

    while (true) {
      List<Message<String>> messages = subscriber.consume(1);
      messages.forEach(msg -> System.out.println("Got message: "+msg));

      if (messages.isEmpty()) {
        ThreadUtils.sleep(1000);
      }
    }
  }
}
