/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.helloworld;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.publisher.HalPublisherWsClient;
import com.microbasecloud.halogen.wrappers.BasicMessage;
import com.microbasecloud.util.GsonFactory;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HelloHalogenPublisher {
  public static void main(String[] args) {
    UUID publisherId = UUID.fromString("00000000-0000-0000-0000-000000000001");
    UUID secretKey = UUID.fromString("00000000-0000-0000-0000-000000000001");

    HalPublisher<String> publisher = new HalPublisherWsClient(GsonFactory.make(), "http://localhost:5001", publisherId, secretKey);

    Message<String> message = new BasicMessage<>();
    message.getHeader().setChannel("channel1");
    message.getHeader().setTopic("topic1");
    message.getHeader().setType("String");

    message.setContent("Hello world!");

    publisher.publish(message);
  }
}
