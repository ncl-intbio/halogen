/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen;

import java.util.Date;
import java.util.UUID;

/**
 * This interface defines the meta-data that we need to store along with the message content.
 * Halogen doesn't care about the message content, but does need to know how to lookup messages,
 * and decide which subscribers a message is relevant to.
 *
 * The message header contains:
 * <ul>
 *   <li>A unique message identifier</li>
 *   <li>An optional identifier of a parent message (i.e. <code>this</code> message is a
 *   response to a previous message)</li>
 *   <li>The ID of the actor that published this message (e.g., an account ID or API key)</li>
 *   <li>A 'channel' is a form of interest management that allows multiple actors to send and receive messages
 *   amongst each-other. Actors that subscribe to a channel will receive all messages posted to that channel.</li>
 *   <li>A 'topic' is analogous to an email 'subject line, and is another form of interest management.
 *   It is effectively a 'conversation id'.</li>
 *   <li>The message 'type' is both a form of interest management, and also a means to understand the actual
 *   payload of the message. A subscriber may not be interested in all message types. If it is interested, it can
 *   use the messgae type field to decide how to parse the content.</li>
 * </ul>
 *
 * @author Keith Flanagan
 */
public class MessageHeader {
  private UUID uid;
  private UUID parentId;

  private UUID publisherId;
  private String channel;
  private Date date;
  private String topic;
  private String type;

  public MessageHeader() {
    this.uid = UUID.randomUUID();
    this.date = new Date();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MessageHeader that = (MessageHeader) o;

    return uid.equals(that.uid);
  }

  @Override
  public int hashCode() {
    return uid.hashCode();
  }

  @Override
  public String toString() {
    return "MessageHeader{" +
        "uid=" + uid +
        ", parentId=" + parentId +
        ", publisherId=" + publisherId +
        ", channel='" + channel + '\'' +
        ", date=" + date +
        ", topic='" + topic + '\'' +
        ", type='" + type + '\'' +
        '}';
  }


  public UUID getUid() {
    return uid;
  }


  public void setUid(UUID uid) {
    this.uid = uid;
  }


  public UUID getParentId() {
    return parentId;
  }


  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }


  public UUID getPublisherId() {
    return publisherId;
  }


  public void setPublisherId(UUID publisherId) {
    this.publisherId = publisherId;
  }


  public String getChannel() {
    return channel;
  }


  public void setChannel(String channel) {
    this.channel = channel;
  }


  public Date getDate() {
    return date;
  }


  public void setDate(Date date) {
    this.date = date;
  }


  public String getTopic() {
    return topic;
  }


  public void setTopic(String topic) {
    this.topic = topic;
  }


  public String getType() {
    return type;
  }


  public void setType(String type) {
    this.type = type;
  }
}
