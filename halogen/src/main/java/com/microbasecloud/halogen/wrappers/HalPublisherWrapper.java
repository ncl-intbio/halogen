/*
 * Copyright 2017 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.microbasecloud.halogen.wrappers;

import com.google.gson.Gson;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.store.HalogenStorageException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HalPublisherWrapper<T> implements HalPublisher<T> {

  private final HalPublisher<String> delegate;
  private final DefaultWrappedMessageParser parser;

  public HalPublisherWrapper(Gson gson, HalPublisher<String> delegate) {
    this.delegate = delegate;
    this.parser = new DefaultWrappedMessageParser(gson);
  }

  @Override
  public void publish(Message<T> message) throws HalogenStorageException {
    publishAll(List.of(message));
  }

  @Override
  public void publishAll(List<Message<T>> messages) throws HalogenStorageException {
    List<Message<String>> encodedMessages = new ArrayList<>();
    for (Message message : messages) {
      Message<String> encoded = parser.encode(message);
      encodedMessages.add(encoded);
    }
    delegate.publishAll(encodedMessages);
  }

  @Override
  public boolean isRegistered() throws HalogenStorageException {
    return delegate.isRegistered();
  }

  @Override
  public List<Message<T>> listAllPublisherResponsesTo(UUID messageId, int maxMsgs, boolean ascending) throws HalogenStorageException {
    return parser.decode(delegate.listAllPublisherResponsesTo(messageId, maxMsgs, ascending));
  }

  @Override
  public Message<T> lastMessagePublisherSent() throws HalogenStorageException {
    return parser.decode(delegate.lastMessagePublisherSent());
  }

  @Override
  public Message<T> lastMessagePublisherRespondedTo() throws HalogenStorageException {
    return parser.decode(delegate.lastMessagePublisherRespondedTo());
  }
}
