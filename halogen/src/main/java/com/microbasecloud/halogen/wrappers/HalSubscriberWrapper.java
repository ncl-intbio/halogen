package com.microbasecloud.halogen.wrappers;

import com.google.gson.Gson;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.store.HalogenStorageException;
import com.microbasecloud.halogen.subscriber.HalSubscriber;

import java.util.List;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HalSubscriberWrapper implements HalSubscriber {

  private final HalSubscriber<String> delegate;
  private final DefaultWrappedMessageParser parser;

  public HalSubscriberWrapper(Gson gson, HalSubscriber<String> delegate) {
    this.delegate = delegate;
    this.parser = new DefaultWrappedMessageParser(gson);
  }

  @Override
  public List<Message> consume(int maxMsgs, boolean advancePointer) throws HalogenStorageException {
    return parser.decode(delegate.consume(maxMsgs, advancePointer));
  }

  @Override
  public List<Message> consumeAfter(UUID messagePointer, int maxMsgs, boolean advancePointer) throws HalogenStorageException {
    return parser.decode(delegate.consumeAfter(messagePointer, maxMsgs, advancePointer));
  }

  @Override
  public void reset() throws HalogenStorageException {
    setMessagePointer(null);
  }

  @Override
  public UUID stepBack() throws HalogenStorageException {
    return delegate.stepBack();
  }

  @Override
  public void setMessagePointer(UUID messageId) throws HalogenStorageException {
    delegate.setMessagePointer(messageId);
  }

  @Override
  public boolean isRegistered() throws HalogenStorageException {
    return delegate.isRegistered();
  }
}
