/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halogen.wrappers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.Message;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Keith Flanagan
 */
public class DefaultWrappedMessageParser<T> {
  // The provided Gson instance must have a ContentWrapperMarshaller registered for this to work correctly
  private final Gson gson;

  public DefaultWrappedMessageParser(Gson gson) {
    this.gson = gson;
  }

  public Message<String> encode(Message message) {
    Message<String> stringMessage = new BasicMessage<>();
    stringMessage.setHeader(message.getHeader());
    stringMessage.setContent(gson.toJson(new ContentWrapper<>(message.getContent())));

    return stringMessage;
  }

  public Message<T> decode(Message<String> stringMessage) {
    // The provided Gson instance must have a ContentWrapperMarshaller registered for this to work correctly
    Message<T> message = new BasicMessage<>();
    message.setHeader(stringMessage.getHeader());
    //eventMessage.setContent(gson.fromJson(stringMessage.getContent(), ContentWrapper.class).getContent());
    ContentWrapper<T> wrappedPayload = gson.fromJson(stringMessage.getContent(), new TypeToken<ContentWrapper<T>>(){}.getType());
    message.setContent(wrappedPayload.getContent());
    return message;
  }

  public List<Message<T>> decode(List<Message<String>> stringMessages) {
    return stringMessages.stream().map(this::decode).collect(Collectors.toList());
  }
}
