/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.halogen.wrappers;

import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.MessageHeader;

/**
 * @author Keith Flanagan
 */
public class BasicMessage<T> implements Message<T> {
  private MessageHeader header;
  private T content;

  public BasicMessage() {
    header = new MessageHeader();
  }

  public BasicMessage(MessageHeader header, T content) {
    this.header = header;
    this.content = content;
  }

  @Override
  public MessageHeader getHeader() {
    return header;
  }

  @Override
  public void setHeader(MessageHeader header) {
    this.header = header;
  }

  @Override
  public T getContent() {
    return content;
  }

  @Override
  public void setContent(T content) {
    this.content = content;
  }
}
