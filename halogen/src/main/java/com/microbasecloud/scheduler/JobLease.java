/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.scheduler;

/**
 * @author Keith Flanagan
 */
public class JobLease {
  private JobDescription jobDescription;
  private String leasedBy;
  private long leasedForMs;
  private long leaseExpiresAt;

  public JobDescription getJobDescription() {
    return jobDescription;
  }

  public void setJobDescription(JobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }

  public String getLeasedBy() {
    return leasedBy;
  }

  public void setLeasedBy(String leasedBy) {
    this.leasedBy = leasedBy;
  }

  public long getLeasedForMs() {
    return leasedForMs;
  }

  public void setLeasedForMs(long leasedForMs) {
    this.leasedForMs = leasedForMs;
  }

  public long getLeaseExpiresAt() {
    return leaseExpiresAt;
  }

  public void setLeaseExpiresAt(long leaseExpiresAt) {
    this.leaseExpiresAt = leaseExpiresAt;
  }
}
