/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.scheduler;

import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class JobDescription {
  private UUID jobId;
  private String[] commandLine;

  public JobDescription() {
  }

  public JobDescription(UUID jobId, String... commandLine) {
    this.jobId = jobId;
    this.commandLine = commandLine;
  }

  public UUID getJobId() {
    return jobId;
  }

  public void setJobId(UUID jobId) {
    this.jobId = jobId;
  }

  public String[] getCommandLine() {
    return commandLine;
  }

  public void setCommandLine(String[] commandLine) {
    this.commandLine = commandLine;
  }
}
