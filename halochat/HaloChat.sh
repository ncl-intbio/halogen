#!/bin/bash

EXE="com.microbasecloud.halochat.HaloChatLauncher"
RAM="-Xmx256M"
#RAM="-Xmx1024M"

CP="target/dependency/*"
CP="$CP:target/halochat-1.0-SNAPSHOT.jar"


# Debug
#java $RAM \
#    -XX:+UnlockCommercialFeatures -XX:+FlightRecorder -Dcom.sun.management.jmxremote \
#    -Dcom.sun.management.jmxremote.port=6062 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false \
#    -cp $CP $EXE $@ 

java $RAM -cp $CP $EXE $@ 


