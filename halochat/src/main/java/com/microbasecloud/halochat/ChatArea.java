/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;

import com.microbasecloud.halogen.StringMessage;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Keith Flanagan
 */
public class ChatArea extends JPanel {

  private final State state;
  private final JTabbedPane channelTabsHolder;

  private final Map<String, JTextArea> tabs;

  //private final Map<String, Subscription> subscriptions;

  public ChatArea(State state) {
    this.state = state;

    setLayout(new CardLayout());
    channelTabsHolder = new JTabbedPane(JTabbedPane.BOTTOM);
    add(channelTabsHolder);

    tabs = new HashMap<>();
  }

  public JTextArea addChannelTab(String channel) {
    JTextArea channelText = new JTextArea();
    tabs.put(channel, channelText);
    JScrollPane scoller = new JScrollPane(channelText);
    channelTabsHolder.addTab(channel, scoller);

    return channelText;
  }

  public void addChannelText(String channel, StringMessage toAdd) {
    JTextArea channelText = tabs.get(channel);
    if (channelText == null) {
      channelText = addChannelTab(channel);
    }

    channelText.setText(channelText.getText()+"\n"
        +toAdd.getHeader().getPublisherId().toString()+"/"
        +toAdd.getHeader().getType()
        +": "+toAdd.getContent());
  }
}
