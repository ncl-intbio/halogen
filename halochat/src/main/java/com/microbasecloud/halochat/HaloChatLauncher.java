/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;

import com.microbasecloud.halogen.StringMessage;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.publisher.HalPublisherWsClient;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.HalSubscriberWsClient;

import javax.swing.*;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class HaloChatLauncher {
  //private static final String WS_URL_BASE = "http://neptune.bioswarm.net";
  private static final String WS_URL_BASE = "http://polen.ico2s.org";
  private static final String SUBSCRIBER_URL = WS_URL_BASE+":5000";
  private static final String PUBLISHER_URL = WS_URL_BASE+":5001";

  public static void main(String[] args) {
    JFrame frame = new JFrame("Hal0Chat!");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    frame.setSize(1024, 768);

    UUID publisherId = UUID.randomUUID();
    UUID subscriberId = UUID.randomUUID();


    HalSubscriber<StringMessage> subscriber = new HalSubscriberWsClient(SUBSCRIBER_URL);
    HalPublisher<StringMessage> publisher = new HalPublisherWsClient(PUBLISHER_URL);

    State state = new State(publisherId, subscriberId, publisher, subscriber);
    HaloChat haloChat = new HaloChat(state);

    frame.setContentPane(haloChat);

    frame.setVisible(true);

    System.out.println("Foo!");
  }
}
