/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;

import com.microbasecloud.halogen.StringMessage;

import javax.swing.*;
import java.awt.*;

/**
 * @author Keith Flanagan
 */
public class InputBoxPanel extends JPanel {

  private final State state;

  private final JTextField channel;
  private final JTextField topic;
  private final JTextField type;

  private final JTextArea input;
  private final JButton send;


  public InputBoxPanel(State state) {
    this.state = state;

    setLayout(new BorderLayout());


    JPanel central = new JPanel(new GridLayout(2, 1));

    JPanel configPanel = new JPanel();

    channel = new JTextField("#intbio");
    topic = new JTextField("IRC over halogen");
    type = new JTextField("text");

    configPanel.add(new JLabel("Channel: "));
    configPanel.add(channel);

    configPanel.add(new JLabel("Topic: "));
    configPanel.add(topic);

    configPanel.add(new JLabel("Type: "));
    configPanel.add(type);

    central.add(configPanel);

    input = new JTextArea();
    central.add(input);

    send = new JButton("Send!");

    add(central, BorderLayout.CENTER);
    add(send, BorderLayout.EAST);

    send.addActionListener(e -> {
      StringMessage message = new StringMessage();
      message.getHeader().setTopic(topic.getText());
      message.getHeader().setType(type.getText());
      message.getHeader().setChannel(channel.getText());

      message.setContent(input.getText());

      try {
        System.out.println("Sending");
        state.getPublisher().publish(state.getPublisherId(), state.getPublisherId(), message);
        System.out.println("Sent!");
      } catch (Exception ex) {
        ex.printStackTrace();
      }

      input.setText("");
    });
  }
}
