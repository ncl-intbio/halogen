/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;

import com.microbasecloud.halogen.subscriber.Subscription;

import javax.swing.*;
import java.awt.*;

/**
 * @author Keith Flanagan
 */
public class SubscriberBoxPanel extends JPanel {

  private final State state;
  private final JTextField channel;
  private final JButton subscribe;


  public SubscriberBoxPanel(State state) {
    this.state = state;

    setLayout(new FlowLayout());


    channel = new JTextField("#intbio");
    subscribe = new JButton("Subscribe!");

    add(new JLabel("Channel to subscribe to:"));
    add(channel);
    add(subscribe);


    subscribe.addActionListener(e -> {
      if (state.getChannelToSubscription().containsKey(channel.getText())) {
        return;
      }

      Subscription subscription = new Subscription();
      subscription.setSubscriptionType(Subscription.Type.CHANNEL);
      subscription.setChannel(channel.getText());

      state.getChannelToSubscription().put(channel.getText(), subscription);
      System.out.println("Added subscription to: "+channel.getText());
    });
  }
}
