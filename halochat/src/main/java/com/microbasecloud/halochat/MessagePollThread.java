/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;

import com.microbasecloud.halogen.StringMessage;
import com.microbasecloud.halogen.subscriber.Subscription;

import java.util.List;
import java.util.Map;

/**
 * @author Keith Flanagan
 */
public class MessagePollThread implements Runnable {
  private final State state;
  private final ChatArea chatArea;

  public MessagePollThread(State state, ChatArea chatArea) {
    this.state = state;
    this.chatArea = chatArea;
  }

  @Override
  public void run() {
    try {
//      System.out.println("Doing message poll");

      for (Map.Entry<String, Subscription> entry : state.getChannelToSubscription().entrySet()) {
        List<StringMessage> msgs = state.getSubscriber().consume(state.getSubscriberId(), state.getSubscriberId(), entry.getValue(), 10);

        if (!msgs.isEmpty()) {
          System.out.println("Got: " + msgs.size() + " for subscription: " + entry.getValue());
        }

        msgs.forEach(stringMessage -> {
          chatArea.addChannelText(entry.getKey(), stringMessage);
        });
      }

//      System.out.println("Done message poll.");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
