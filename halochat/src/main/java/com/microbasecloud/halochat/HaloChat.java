/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;


import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Keith Flanagan
 */
public class HaloChat extends JPanel {

  private State state;

  private SubscriberBoxPanel subscriberBoxPanel;
  private ChatArea chatArea;
  private InputBoxPanel inputBoxPanel;

  private final ScheduledThreadPoolExecutor exe;

  public HaloChat(State state) {
    this.exe = new ScheduledThreadPoolExecutor(1);
    this.state = state;

    subscriberBoxPanel = new SubscriberBoxPanel(state);
    chatArea = new ChatArea(state);
    inputBoxPanel = new InputBoxPanel(state);

    setLayout(new BorderLayout());

    add(subscriberBoxPanel, BorderLayout.NORTH);

    add(chatArea, BorderLayout.CENTER);
    add(inputBoxPanel, BorderLayout.SOUTH);

    MessagePollThread messagePollThread = new MessagePollThread(state, chatArea);
    exe.scheduleWithFixedDelay(messagePollThread, 1000L, 1000L, TimeUnit.MILLISECONDS);
  }
}
