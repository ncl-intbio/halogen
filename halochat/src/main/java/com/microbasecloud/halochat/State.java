/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.microbasecloud.halochat;

import com.microbasecloud.halogen.StringMessage;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.subscriber.HalSubscriber;
import com.microbasecloud.halogen.subscriber.Subscription;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Keith Flanagan
 */
public class State {

  private final UUID publisherId;
  private final UUID subscriberId;

  private final HalPublisher<StringMessage> publisher;
  private final HalSubscriber<StringMessage> subscriber;

  private final Map<String, Subscription> channelToSubscription;

  public State(UUID publisherId, UUID subscriberId, HalPublisher<StringMessage> publisher, HalSubscriber<StringMessage> subscriber) {
    this.publisherId = publisherId;
    this.subscriberId = subscriberId;
    this.publisher = publisher;
    this.subscriber = subscriber;

    this.channelToSubscription = new HashMap<>();
  }

  public UUID getPublisherId() {
    return publisherId;
  }

  public UUID getSubscriberId() {
    return subscriberId;
  }

  public HalPublisher<StringMessage> getPublisher() {
    return publisher;
  }

  public HalSubscriber<StringMessage> getSubscriber() {
    return subscriber;
  }

  public Map<String, Subscription> getChannelToSubscription() {
    return channelToSubscription;
  }
}
