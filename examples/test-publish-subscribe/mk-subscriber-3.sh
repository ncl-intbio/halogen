#!/bin/bash

# A subscriber that should receive all messages for the specified channel, regardless of topic or type

type="Type_2"

docker run -it --rm \
  --net halogen_demo \
  --link halogendemo_haladmin_1:haladmin \
  keithf/halogen-amd64:latest \
  /halogen/MakeSubscriber.sh \
    --url http://haladmin:5000 \
    --adminId     00000000-0000-0000-0000-000000000000 \
    --adminSecret 00000000-0000-0000-0000-000000000000 \
    --newAccountId     00000000-0000-0000-0000-000000000003 \
    --newAccountSecret 00000000-0000-0000-0000-000000000000 \
    --type ${type} 



