#!/bin/bash

# A subscriber that should receive ALL messages, regardless of channel/topic/type

docker run -it --rm \
  --net halogen_demo \
  --link halogendemo_haladmin_1:haladmin \
  keithf/halogen-amd64:latest \
  /halogen/MakePublisher.sh \
    --url http://haladmin:5000 \
    --adminId     00000000-0000-0000-0000-000000000000 \
    --adminSecret 00000000-0000-0000-0000-000000000000 \
    --newAccountId     00000000-0000-0000-0000-000000000000 \
    --newAccountSecret 00000000-0000-0000-0000-000000000000



