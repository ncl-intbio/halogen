#!/bin/bash

# A subscriber that should receive messages only for the specified channel, topic & type

channel="Channel_2"
topic="Topic_2"
type="Type_2"

docker run -it --rm \
  --net halogen_demo \
  --link halogendemo_haladmin_1:haladmin \
  keithf/halogen-amd64:latest \
  /halogen/MakeSubscriber.sh \
    --url http://haladmin:5000 \
    --adminId     00000000-0000-0000-0000-000000000000 \
    --adminSecret 00000000-0000-0000-0000-000000000000 \
    --newAccountId     00000000-0000-0000-0000-000000000001 \
    --newAccountSecret 00000000-0000-0000-0000-000000000000 \
    --channel ${channel} \
    --topic ${topic} \
    --type ${type} 



